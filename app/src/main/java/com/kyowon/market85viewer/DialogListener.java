package com.kyowon.market85viewer;

public interface DialogListener {
    public void onNegativeClicked();

    public void onPositiveClicked();

    public void onClose();
}


