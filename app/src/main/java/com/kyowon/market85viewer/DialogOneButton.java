package com.kyowon.market85viewer;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import androidx.annotation.NonNull;

public class DialogOneButton extends Dialog implements View.OnClickListener {

    private DialogListener dialogListener;

    private Context context;

    private TextView txtMessage;
    private TextView btnNegative, btnPositive;

    private String message = "";
    private String positive = "";

    public DialogOneButton(@NonNull Context context, String message, String positive) {
        super(context);
        this.context = context;
        this.message = message;
        this.positive = positive;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        setContentView(R.layout.dialog_one_button);

        txtMessage = findViewById(R.id.txtMessage);
        txtMessage.setText(message);

        btnPositive = findViewById(R.id.btnPositive);
        btnPositive.setText(positive);
        btnPositive.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()) {
            case R.id.btnPositive :
                dismiss();
                break;
        }
    }

}
