package com.kyowon.market85viewer;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.kyowon.market85viewer.R;

public class DialogTwoButton extends Dialog implements View.OnClickListener {

    private DialogListener dialogListener;

    private Context context;

    private TextView txtMessage;
    private TextView btnNegative, btnPositive;

    private String message = "";
    private String negative= "";
    private String positive = "";

    /*public DialogTwoButton(@NonNull Context context) {
        super(context);
        this.context = context;
    }*/

    public DialogTwoButton(@NonNull Context context, String message, String negative, String positive) {
        super(context);
        this.context = context;
        this.message = message;
        this.negative = negative;
        this.positive = positive;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        setContentView(R.layout.dialog_two_button);

        txtMessage = findViewById(R.id.txtMessage);
        txtMessage.setText(message);

        btnNegative = findViewById(R.id.btnNegative);
        btnNegative.setText(negative);
        btnNegative.setOnClickListener(this);

        btnPositive = findViewById(R.id.btnPositive);
        btnPositive.setText(positive);
        btnPositive.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()) {
            case R.id.btnNegative :
                dismiss();
                break;

            case R.id.btnPositive :
                dialogListener.onPositiveClicked();
                dismiss();
                break;
        }
    }

    public void setDialogListener(DialogListener dialogListener) {
        this.dialogListener = dialogListener;
    }
}
