package com.kyowon.market85viewer;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.ImageButton;

import androidx.annotation.NonNull;

import com.airbnb.lottie.LottieAnimationView;
import com.airbnb.lottie.LottieDrawable;

public class DialogAutoPlayInfo extends Dialog {

    private DialogListener dialogListener;

    private Context context;

    private ImageButton btnClose;
    private LottieAnimationView lottieAnimView;

    private PreferenceManager preferenceManager;

    public DialogAutoPlayInfo(@NonNull Context context) {
        super(context);
        this.context = context;
        preferenceManager = new PreferenceManager();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        setContentView(R.layout.dialog_auto_play_info);

        btnClose = findViewById(R.id.btnClose);
        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                preferenceManager.setBoolean(context, Constants.PREFERENCES_AUTO_PLAY_INFO, true);
                dialogListener.onClose();
                dismiss();
            }
        });


        lottieAnimView = findViewById(R.id.lottieAnimView);
        setUpAnimation(lottieAnimView);

    }

    private void setUpAnimation(LottieAnimationView animationView) {
        animationView.setAnimation("Popup_Character.json");

        animationView.setRepeatCount(LottieDrawable.INFINITE);

        animationView.playAnimation();
    }

    public void setDialogListener(DialogListener dialogListener) {
        this.dialogListener = dialogListener;
    }

}

