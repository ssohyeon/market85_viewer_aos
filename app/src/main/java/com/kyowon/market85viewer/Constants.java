package com.kyowon.market85viewer;

public class Constants {
    public static final String BASE_URL = "https://devapi.market85.com:9443";

    public static final String RECORD = "/svc/subscriptionContentsApi/record"; // 현재 읽고 있는 페이지 갱신

    public static final String BOOKMARK_LIST = "/svc/subscriptionBookmarkApi/bookmarks"; // 북마크 조회
    public static final String BOOKMARK = "/svc/subscriptionBookmarkApi/bookmark"; // 북마크 등록
    public static final String BOOKMARK_DELETE = "/svc/subscriptionBookmarkApi/remove"; // 북마크 삭제

    //public static final String Cookie = "kwopSvcJWT=SDP+eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJzdmMiLCJqdGkiOiIpLVx1MDAxMiAqXHJcIlx0IiwiYXVkIjoiMTcyLjE2LjMxLjEzMSIsImlzcyI6IkktT04iLCJpYXQiOjE1ODk1MTY3MjEsImV4cCI6MzI0NzIxMTE2MDB9.JysyrlMNblLnuZgmBrvQk1U_YCpWib-1K8uoThBNclBqTYCciIiHAFY62qX34rWS6Tz8mLbd1U4cDmUW17KVcw";
   //SDP+eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJzdmMiLCJqdGkiOiJcZilcYlx1MDAwNCYhXHUwMDE4XHUwMDExIiwiYXVkIjoiMTEyLjE3NS44OS4xODYiLCJpc3MiOiJJLU9OIiwiaWF0IjoxNTg5NTE3MTczLCJleHAiOjMyNDcyMTExNjAwfQ.kuHZUsFAIJ5bhrvcq2ybZa8Mt3-jIkGak1VWnzk5ACLoy6hRXGoBFgiFaUr85Fl1703DvEIEhfuI3Joo8v7szg
    public static final String HEADER_KEY = "Cookie";
    public static final String SITE_ID = "kwop";


    public static final int SCREEN_TYPE_DEFAULT = 0;
    public static final int SCREEN_TYPE_COMMON = 1;


    public static final int MEDIA_OVERLAY_MODE_DISABLE = -1;
    public static final int MEDIA_OVERLAY_MODE_OFF = 0;
    public static final int MEDIA_OVERLAY_MODE_ON = 1;

    public static final int MEDIA_STATE_PLAY = 0;
    public static final int MEDIA_STATE_PAUSE = 1;


    public static final String PREFERENCES_NAME = "market85viewer_preference";


    public static final String PREFERENCES_TUTORIAL = "TUTORIAL";

    public static final String PREFERENCES_AUTO_PLAY_INFO = "AUTO_PLAY_INFO";
    public static final String PREFERENCES_MAGAZINE_FIRST_OPEN = "MAGAZINE_FIRST_OPEN";

    public static final String PREFERENCES_DOUBLE_PAGE = "DOUBLE_PAGE";
    public static final String PREFERENCES_CURL_EFFECT = "CURL_EFFECT";
    public static final String PREFERENCES_KEEP_SCREEN_ON = "KEEP_SCREEN_ON";
    public static final String PREFERENCES_LOCK_ROTATION = "LOCK_ROTATION";
    public static final String PREFERENCES_BRIGHTNESS_CONTROL = "BRIGHTNESS_CONTROL";
    public static final String PREFERENCES_AUTO_PLAY = "AUTO_PLAY";

    public static final String PREFERENCES_FIRST_PLAY = "FIRST_PLAY";
}

