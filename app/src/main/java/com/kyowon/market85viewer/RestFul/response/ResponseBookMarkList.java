package com.kyowon.market85viewer.RestFul.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.kyowon.market85viewer.RestFul.item.BookMarkListItem;

import java.util.List;

public class ResponseBookMarkList extends BaseResponse {

    public int getTotalCount() {
        return totalCount;
    }

    public int getResultCount() {
        return resultCount;
    }

    public List<BookMarkListItem> getItems() {
        return items;
    }

    public void setTotalCount(int totalCount) {
        this.totalCount = totalCount;
    }

    public void setResultCount(int resultCount) {
        this.resultCount = resultCount;
    }

    public void setItems(List<BookMarkListItem> items) {
        this.items = items;
    }

    int totalCount;
    int resultCount;

    @SerializedName("items")
    @Expose
    List<BookMarkListItem> items;

}
