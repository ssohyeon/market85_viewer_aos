package com.kyowon.market85viewer.RestFul.request;

public class RequestBookMarkList {
    public String get_siteId() {
        return _siteId;
    }

    public String getContentId() {
        return contentId;
    }

    public int getPage() {
        return page;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void set_siteId(String _siteId) {
        this._siteId = _siteId;
    }

    public void setContentId(String contentId) {
        this.contentId = contentId;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    private String _siteId;
    private String contentId;
    private int page;
    private int pageSize;
}
