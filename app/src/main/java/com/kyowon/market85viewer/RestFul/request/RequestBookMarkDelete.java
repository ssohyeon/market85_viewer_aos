package com.kyowon.market85viewer.RestFul.request;

public class RequestBookMarkDelete {
    public String get_siteId() {
        return _siteId;
    }

    public int getSubscriptionBookmarkNo() {
        return subscriptionBookmarkNo;
    }

    public void set_siteId(String _siteId) {
        this._siteId = _siteId;
    }

    public void setSubscriptionBookmarkNo(int subscriptionBookmarkNo) {
        this.subscriptionBookmarkNo = subscriptionBookmarkNo;
    }

    private String _siteId;
    private int subscriptionBookmarkNo;
}
