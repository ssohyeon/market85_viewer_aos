package com.kyowon.market85viewer.RestFul.API;


import com.kyowon.market85viewer.Constants;
import com.kyowon.market85viewer.RestFul.response.ResponseBookMark;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface BookMarkAPI {
    //String header = Constants.Cookie;

    //@Headers(header)
    @GET(Constants.BOOKMARK)
    Call<ResponseBookMark> getPost(
            @Query("_siteId") String _siteId,
            @Query("contentsId") String contentsId,
            @Query("page") int page,
            @Query("title") String title
    );
}
