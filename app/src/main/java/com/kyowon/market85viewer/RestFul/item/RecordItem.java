package com.kyowon.market85viewer.RestFul.item;

public class RecordItem {
    public String getContentsId() {
        return contentsId;
    }

    public int getRecord() {
        return record;
    }

    public String get_siteId() {
        return _siteId;
    }

    public void setContentsId(String contentsId) {
        this.contentsId = contentsId;
    }

    public void setRecord(int record) {
        this.record = record;
    }

    public void set_siteId(String _siteId) {
        this._siteId = _siteId;
    }

    private String _siteId;
    private String contentsId;
    private int record;
}
