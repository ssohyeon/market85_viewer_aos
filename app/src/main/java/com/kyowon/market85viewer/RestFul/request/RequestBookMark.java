package com.kyowon.market85viewer.RestFul.request;

public class RequestBookMark {

    public String getContentsId() {
        return contentsId;
    }

    public int getPage() {
        return page;
    }

    public String get_siteId() {
        return _siteId;
    }

    public String getTitle() {
        return title;
    }

    public void setContentsId(String contentsId) {
        this.contentsId = contentsId;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public void set_siteId(String _siteId) {
        this._siteId = _siteId;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    private String _siteId;
    private String contentsId;
    private int page;
    private String title;

}
