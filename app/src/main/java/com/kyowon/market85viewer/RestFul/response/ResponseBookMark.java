package com.kyowon.market85viewer.RestFul.response;

public class ResponseBookMark extends BaseResponse {

    public ResponseBookMarkItemObj getItem() {
        return item;
    }

    public void setItem(ResponseBookMarkItemObj item) {
        this.item = item;
    }

    ResponseBookMarkItemObj item;

}
