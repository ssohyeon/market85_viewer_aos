package com.kyowon.market85viewer.RestFul.API;

import com.kyowon.market85viewer.Constants;
import com.kyowon.market85viewer.RestFul.request.RequestRecord;
import com.kyowon.market85viewer.RestFul.response.BaseResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface RecordAPI {
    //String kwopSvcJWT = Constants.Cookie;

    //@Headers({kwopSvcJWT})
    @POST(Constants.RECORD)
    Call<BaseResponse> post_posts(
            @Body RequestRecord requestRecord);

    @GET(Constants.RECORD)
    Call<BaseResponse> getPost(
            @Query("_siteId") String _siteId,
            @Query("contentsId") String contentsId,
            @Query("record") int record);

}
