package com.kyowon.market85viewer.RestFul.API;


import com.kyowon.market85viewer.Constants;
import com.kyowon.market85viewer.RestFul.response.ResponseBookMarkList;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface BookMarkListAPI {
    //String header = Constants.Cookie;

    //@Headers(header)
    @GET(Constants.BOOKMARK_LIST)
    Call<ResponseBookMarkList> getPost(
            @Query("_siteId") String _siteId,
            @Query("contentsId") String contentsId,
            @Query("page") int page,
            @Query("pageSize") int pageSize);
    /*
    @POST("/posts/")
    Call<BookMarkListItem> post_posts(@Body BookMarkListItem bookMarkListItem);

    @PATCH("/posts/{pk}/")
    Call<BookMarkListItem> patch_posts(@Path("pk") int pk, @Body BookMarkListItem bookMarkListItem);

    @DELETE("/posts/{pk}/")
    Call<BookMarkListItem> delete_posts(@Path("pk") int pk);
*/
/*
    @GET("/posts/{pk}/")
    Call<BookMarkListItem> get_post_pk(@Path("pk") int pk);
*/
}
