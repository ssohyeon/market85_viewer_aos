package com.kyowon.market85viewer.RestFul.request;

public class RequestRecord {

    public String get_siteId() {
        return _siteId;
    }

    public String getContentsId() {
        return contentsId;
    }

    public int getRecord() {
        return record;
    }

    public void set_siteId(String _siteId) {
        this._siteId = _siteId;
    }

    public void setContentsId(String contentsId) {
        this.contentsId = contentsId;
    }

    public void setRecord(int record) {
        this.record = record;
    }

    private String _siteId;
    private String contentsId;
    private int record;
}
