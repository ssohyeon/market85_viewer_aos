package com.kyowon.market85viewer.RestFul.API;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.POST;
import retrofit2.http.Streaming;
import retrofit2.http.Url;

public interface Download {
    @Streaming
    @POST()
    Call<ResponseBody> downloadFileByUrl(@Url String fileUrl) ;
}
