package com.kyowon.market85viewer.RestFul.response;

public class ResponseBookMarkItemObj extends Object {
    public int getSubscriptionBookmarkNo() {
        return subscriptionBookmarkNo;
    }

    /*public String getContentsId() {
        return contentsId;
    }

    public String getCustomerNo() {
        return customerNo;
    }*/

    public int getPage() {
        return page;
    }

    public String getCreated() {
        return created;
    }

    public String getTitle() {
        return title;
    }

    public void setSubscriptionBookmarkNo(int subscriptionBookmarkNo) {
        this.subscriptionBookmarkNo = subscriptionBookmarkNo;
    }

    /*public void setContentsId(String contentsId) {
        this.contentsId = contentsId;
    }

    public void setCustomerNo(String customerNo) {
        this.customerNo = customerNo;
    }*/

    public void setPage(int page) {
        this.page = page;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public void setTitle(String titl) {
        this.title = titl;
    }

    private int subscriptionBookmarkNo;
    //private String contentsId;
    //private String customerNo;
    private int page;
    private String created;
    private String title;
}
