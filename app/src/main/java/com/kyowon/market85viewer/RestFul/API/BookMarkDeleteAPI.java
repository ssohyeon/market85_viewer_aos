package com.kyowon.market85viewer.RestFul.API;

import com.kyowon.market85viewer.Constants;
import com.kyowon.market85viewer.RestFul.response.BaseResponse;

import retrofit2.Call;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface BookMarkDeleteAPI {

/*    @POST(Constants.BOOKMARK_DELETE)
    Call<BaseResponse> post_posts(@Body RequestBookMarkDelete requestBookMarkDelete);*/

    @POST(Constants.BOOKMARK_DELETE)
    Call<BaseResponse> post_posts(@Query("_siteId") String _siteId, @Query("subscriptionBookmarkNo") int subscriptionBookmarkNo);
}
