package com.kyowon.market85viewer;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.kyowon.market85viewer.Magazine.MagazineActivity;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

public class TutorialActivity extends Activity {

    public ViewPager viewPagerTutorial;
    public TutorialPagerAdapter tutorialPagerAdapter;

    public ImageButton btnClose;
    public TextView btnStart;
    public ImageView dot_page1, dot_page2, dot_page3;

    private boolean isFirst = false;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

        setContentView(R.layout.activity_tutorial);

        CommonUtil.makeFullscreen(this);

        isFirst = getIntent().getBooleanExtra("isFirst", false);

        PreferenceManager preferenceManager = new PreferenceManager();
        preferenceManager.setBoolean(this, Constants.PREFERENCES_TUTORIAL, true);

        viewPagerTutorial = findViewById(R.id.viewPagerTutorial);

        tutorialPagerAdapter = new TutorialPagerAdapter(this);
        viewPagerTutorial.setAdapter(tutorialPagerAdapter);

        btnClose = findViewById(R.id.btnClose);
        btnStart = findViewById(R.id.btnStart);

        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                if(isFirst) {
                    Intent intent = new Intent(TutorialActivity.this, MagazineActivity.class);
                    intent.putExtra("title", getIntent().getStringExtra("title"));
                    startActivity(intent);
                }
            }
        });

        btnStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                if(isFirst) {
                    Intent intent = new Intent(TutorialActivity.this, MagazineActivity.class);
                    intent.putExtra("title", getIntent().getStringExtra("title"));
                    startActivity(intent);
                }
            }
        });

        dot_page1 = findViewById(R.id.dot_page1);
        dot_page2 = findViewById(R.id.dot_page2);
        dot_page3 = findViewById(R.id.dot_page3);

        viewPagerTutorial.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if(position==0) {
                    dot_page1.setImageDrawable(getResources().getDrawable(R.drawable.tutorial_indicator_dot_selector));
                    dot_page2.setImageDrawable(getResources().getDrawable(R.drawable.tutorial_indicator_dot));
                    dot_page3.setImageDrawable(getResources().getDrawable(R.drawable.tutorial_indicator_dot));
                    btnStart.setVisibility(View.GONE);
                } else if(position==1) {
                    dot_page1.setImageDrawable(getResources().getDrawable(R.drawable.tutorial_indicator_dot));
                    dot_page2.setImageDrawable(getResources().getDrawable(R.drawable.tutorial_indicator_dot_selector));
                    dot_page3.setImageDrawable(getResources().getDrawable(R.drawable.tutorial_indicator_dot));
                    btnStart.setVisibility(View.GONE);
                } else if(position==2) {
                    dot_page1.setImageDrawable(getResources().getDrawable(R.drawable.tutorial_indicator_dot));
                    dot_page2.setImageDrawable(getResources().getDrawable(R.drawable.tutorial_indicator_dot));
                    dot_page3.setImageDrawable(getResources().getDrawable(R.drawable.tutorial_indicator_dot_selector));

                    if(isFirst) {
                        btnStart.setVisibility(View.VISIBLE);
                    } else {
                        btnStart.setVisibility(View.GONE);
                    }
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }



    public class TutorialPagerAdapter extends androidx.viewpager.widget.PagerAdapter {
        public Context context;

        public TutorialPagerAdapter(Context context) {
            this.context = context;
        }

        @Override
        public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
            container.removeView((View) object);
        }

        @NonNull
        @Override
        public Object instantiateItem(@NonNull ViewGroup container, int position) {
            View view = null;

            if(context != null) {
                LayoutInflater inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
                view = inflater.inflate(R.layout.tutorial_page1, container, false);

                ImageView imageViewTutorial = view.findViewById(R.id.imageViewTutorial);
                if(position==0) {
                    imageViewTutorial.setImageDrawable(context.getResources().getDrawable(R.drawable.img_viewer_tutorial_p_1));
                } else if (position == 1) {
                    imageViewTutorial.setImageDrawable(context.getResources().getDrawable(R.drawable.img_viewer_tutorial_p_2));
                } else if (position == 2) {
                    imageViewTutorial.setImageDrawable(context.getResources().getDrawable(R.drawable.img_viewer_tutorial_p_3));
                }

            }

            container.addView(view);

            return view;
        }

        @Override
        public int getCount() {
            return 3;
        }

        @Override
        public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
            return (view == (View)object);
        }


    }
}
