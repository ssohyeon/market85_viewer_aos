package com.kyowon.market85viewer;

public class BookmarkInfo {
    private int page;
    private int subscriptionBookmarkNo;
    private String title;

    public int getPage() {
        return page;
    }

    public int getSubscriptionBookmarkNo() {
        return subscriptionBookmarkNo;
    }

    public String getTitle() {
        return title;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public void setSubscriptionBookmarkNo(int subscriptionBookmarkNo) {
        this.subscriptionBookmarkNo = subscriptionBookmarkNo;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
