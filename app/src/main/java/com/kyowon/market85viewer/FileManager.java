package com.kyowon.market85viewer;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

import java.io.File;
import java.io.FileOutputStream;

public class FileManager {

    Context context;

    public FileManager(Context context) {
        this.context = context;
    }

    public String bookFileName(String contentsId) {
        String fileName = contentsId + ".epub";

        return fileName;
    }

    public String thumbnailFileName(String contentsId, int page) {
        String fileName = contentsId + "_" + page + ".jpg";

        return fileName;
    }

    public String appDirectory() {
        String directory = context.getFilesDir().getAbsolutePath();

        return directory;
    }

    public String viewerDirectory(String contentsId) {
        String directory = appDirectory() + "/" + contentsId;

        return directory;
    }

    public String bookDirectory(String contentsId) {
        String directory = viewerDirectory(contentsId) + "/" + "book";

        return directory;
    }

    public String bookFilePath(String contentsId) {
        String filePath = bookDirectory(contentsId) + "/" + bookFileName(contentsId);

        return filePath;
    }

    public String thumbnailDirectory(String cotentsId) {
        String directory = viewerDirectory(cotentsId) + "/" + "thumbnail";

        return directory;
    }

    public String thumbnailFilePath(String contentsId, int page) {
        String filePath = thumbnailDirectory(contentsId) + "/" + thumbnailFileName(contentsId, page);

        return filePath;
    }

    public void makeFileDirectory(String directory) {
        File dir = new File(directory);
        if(!dir.exists()) {
            dir.mkdir();
        }
    }

    public boolean isBookSaved(String contentsId) {
        String directory = bookDirectory(contentsId);

        File file = new File(directory);
        File fileList[] = file.listFiles();

        if (fileList != null && fileList.length != 0) {
            for (File item : fileList) {
                String fileName = item.getName();

                if (fileName.contains(".epub"))
                    return true;
            }
        }

        return false;
    }

    public boolean isThumbnailSaved(String contentsId) {
        String directory = thumbnailDirectory(contentsId);

        File file = new File(directory);
        File fileList[] = file.listFiles();

        if (fileList != null && fileList.length != 0) {
            for (File item : fileList) {
                String fileName = item.getName();

                if (fileName.contains(".jpg"))
                    return true;
            }
        }

        return false;
    }

    public void saveBitmap(Bitmap bitmap, String filePath) {
        try {
            if (bitmap==null) return;
            FileOutputStream out = new FileOutputStream(filePath);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, out);
            out.flush();
            out.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Bitmap getBitmap(int pageIndex) {
        try {
            Bitmap bitmap = null;
            String filePath = thumbnailFilePath(BookInfo.getContentsId(), pageIndex);
            File file = new File(filePath);
            if (file.exists()) {
                // uses bitmap inside thumbnail rather than inside file because of speed.
//		    	bitmap = getBitmpFromThumbnail(pageIndex);
                if (bitmap==null) {
                    bitmap =  this.getBitmap(filePath);
                }
                Bitmap target = bitmap.copy(bitmap.getConfig(), true);
                return target;
            }else {
                return null;
            }
        }catch(Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private Bitmap getBitmap(String filePath) {
        Bitmap bitmap = null;
        try {
            File file = new File(filePath);
            if (file.exists()) {
                bitmap = BitmapFactory.decodeFile(filePath);
            }else {
                Log.w("FileManager",filePath + " does not exist");
            }
        }catch(Exception e) {
            e.printStackTrace();
        }
        return bitmap;
    }



}
