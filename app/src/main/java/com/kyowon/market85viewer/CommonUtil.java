package com.kyowon.market85viewer;

import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.View;
import android.view.WindowManager;

public class CommonUtil {

    public static float dpToPx(Context context, float dp) {
        DisplayMetrics dm = context.getResources().getDisplayMetrics();

        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, dm);
    }

    public static int pxToDp(Context context, int px) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        return Math.round(px / (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
    }

    public static void makeFullscreen(Activity activity) {
        activity.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        if (Build.VERSION.SDK_INT>=19) {
            activity.getWindow().getDecorView().setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_IMMERSIVE
                            |	View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                            |	View.SYSTEM_UI_FLAG_FULLSCREEN
                            |	View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                            |	View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                            |	View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                            |	View.SYSTEM_UI_FLAG_LAYOUT_STABLE
            );
        }else if (Build.VERSION.SDK_INT>=11) {
            activity.getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LOW_PROFILE);
        }
    }

}
