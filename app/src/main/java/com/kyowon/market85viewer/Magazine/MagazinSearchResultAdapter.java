package com.kyowon.market85viewer.Magazine;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.BackgroundColorSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.kyowon.market85viewer.FileManager;
import com.kyowon.market85viewer.R;
import com.skytree.epub.FixedControl;
import com.skytree.epub.SearchResult;

import java.io.File;
import java.util.ArrayList;

public class MagazinSearchResultAdapter extends RecyclerView.Adapter<MagazinSearchResultAdapter.SearchResultViewHolder> {

    private Context context;
    private ArrayList<SearchResult> searchResults;
    private String searchWord;

    private FileManager fileManager;

    public FixedControl fv;

    public MagazinSearchResultAdapter(Context context, ArrayList<SearchResult> searchResults, String searchWord) {
        this.context = context;
        this.searchResults = searchResults;
        this.searchWord = searchWord;

        this.fv = MagazineFixedControl.getInstance(context);
    }

    @NonNull
    @Override
    public SearchResultViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        fileManager = new FileManager(context);

        View view = LayoutInflater.from(context).inflate(R.layout.search_list_item, parent, false);
        SearchResultViewHolder viewHolder = new SearchResultViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull SearchResultViewHolder holder, int position) {

        String searchResultText = searchResults.get(position).text;
        searchResultText = searchResultText.replace("\n", "");

        int startIndex = searchResultText.indexOf(searchWord);
        int endIndex = startIndex + searchWord.length();

        Log.d("Activity", "SH Test - search startIndex : " + startIndex);
        Log.d("Activity", "SH Test - search endIndex : " + endIndex);

        if(startIndex >= 0 && endIndex <= searchResultText.length()) {
            Spannable WordtoSpan = new SpannableString(searchResultText);
            WordtoSpan.setSpan(new BackgroundColorSpan(context.getResources().getColor(R.color.searchHighlight)), startIndex, endIndex, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

            holder.txtSearchResult.setText(WordtoSpan);
        } else {
            holder.txtSearchResult.setText(searchResultText);
        }

        //int pageIndex = searchResults.get(position).numberOfChaptersInBook;
        int pageIndex = searchResults.get(position).chapterIndex;
        holder.txtSearchResultPage.setText(pageIndex + "페이지");

        Bitmap bitmap = holder.getBitmap(fileManager.thumbnailFilePath("113", pageIndex));
        holder.imgViewThumbnail.setImageBitmap(bitmap);
    }

    @Override
    public int getItemCount() {
        int count = 0;

        if(searchResults != null)
            count = searchResults.size();

        return count;
    }


    public class SearchResultViewHolder extends RecyclerView.ViewHolder {

        ImageView imgViewThumbnail;
        TextView txtSearchResult, txtSearchResultPage;

        public SearchResultViewHolder(@NonNull View itemView) {
            super(itemView);

            imgViewThumbnail = itemView.findViewById(R.id.imgViewThumbnail);
            txtSearchResult = itemView.findViewById(R.id.txtSearchResult);
            txtSearchResultPage = itemView.findViewById(R.id.txtSearchResultPage);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    fv.gotoPage(searchResults.get(getLayoutPosition()).chapterIndex);
                    ((Activity)context).finish();
                    ((Activity)context).overridePendingTransition(R.anim.stay, R.anim.sliding_down);

                }
            });
        }

        public Bitmap getBitmap(String filePath) {
            Bitmap bitmap = null;
            try {
                File file = new File(filePath);
                if (file.exists()) {
                    bitmap = BitmapFactory.decodeFile(filePath);
                }
            }catch(Exception e) {
                e.printStackTrace();
            }
            return bitmap;
        }
    }
}
