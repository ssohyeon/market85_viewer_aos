package com.kyowon.market85viewer.Magazine;

import android.content.Context;

import com.kyowon.market85viewer.Constants;
import com.skytree.epub.FixedControl;
import com.skytree.epub.ItemRef;
import com.skytree.epub.Parallel;

import java.util.ArrayList;

public class MagazineMediaController {

    private int mediaOverlayMode = -1;
    private int prevMediaPlayState = -1;

    private FixedControl fv;

    public MagazineMediaController(Context context) {

        // TODO : setting에서 설정한 값 가져오기 / 오디오 북 자동 재생
        mediaOverlayMode = Constants.MEDIA_OVERLAY_MODE_ON;
        fv = MagazineFixedControl.getInstance(context);
    }


    public boolean hasMediaOverlay() {
        boolean ret = false;

        ArrayList<ItemRef> spine = fv.getBook().spine;

        for(int i=0; i<fv.book.spine.size(); i++) {
            ItemRef itemRef = spine.get(i);

            if(itemRef.hasMediaOverlay)
                ret = true;
        }

        return ret;
    }

    public void mediaOverlayPlayStart() {
        setMediaOverlayMode(Constants.MEDIA_OVERLAY_MODE_ON);
        fv.playFirstParallel();
    }

    public void mediaOverlayPlay() {
        setMediaOverlayMode(Constants.MEDIA_OVERLAY_MODE_ON);
        fv.resumePlayingParallel();
    }

    public void mediaOverlayPause() {
        setMediaOverlayMode(Constants.MEDIA_OVERLAY_MODE_OFF);
        fv.pausePlayingParallel();
    }

    public void mediaOverlayPlayStop() {
        setMediaOverlayMode(Constants.MEDIA_OVERLAY_MODE_OFF);
        fv.stopPlayingParallel();
        fv.restoreElementColor();
    }

    public void mediaPlayResume() {
        if(mediaOverlayMode == Constants.MEDIA_OVERLAY_MODE_ON && prevMediaPlayState == Constants.MEDIA_STATE_PLAY) {
            fv.resumePlayingParallel();
        }
    }

    public void mediaPlayPause() {
        if(mediaOverlayMode == Constants.MEDIA_OVERLAY_MODE_ON) {
            if (fv.isPlayingPaused()) { // 재생 중
                prevMediaPlayState = Constants.MEDIA_STATE_PAUSE;
            } else {
                prevMediaPlayState = Constants.MEDIA_STATE_PLAY;
                fv.pausePlayingParallel();
            }
        }
    }

    public void setMediaPlayState() {
        if(fv.isPlayingPaused()) { // 재생 중 -> 정지 시키기
            fv.resumePlayingParallel();
        } else { // 재생 중 아닐 때 -> 재생 시키기
            fv.pausePlayingParallel();
        }
    }

    public void mediaOverlayPlayPrev(Parallel currentParallel, boolean autoPlay){
        fv.restoreElementColor();
        if (currentParallel.parallelIndex==0) {
            if (autoPlay) {
                fv.gotoPrevPage();
            }
        }else {
            fv.playPrevParallel();
        }
    }

    public void mediaOverlayPlayNext() {
        fv.restoreElementColor();
        fv.playNextParallel();

    }

    private void setMediaOverlayMode (int mediaOverlayMode) {
        this.mediaOverlayMode = mediaOverlayMode;
    }

    public int mediaOverlayMode() {
        if (fv.isMediaOverlayAvailable() || hasMediaOverlay())
            return this.mediaOverlayMode;
        else
            return Constants.MEDIA_OVERLAY_MODE_DISABLE;
    }
}
