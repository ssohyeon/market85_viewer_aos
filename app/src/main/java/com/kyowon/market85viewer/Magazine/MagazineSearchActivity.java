package com.kyowon.market85viewer.Magazine;

import android.app.Activity;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.kyowon.market85viewer.CommonUtil;
import com.kyowon.market85viewer.DialogOneButton;
import com.kyowon.market85viewer.R;
import com.skytree.epub.FixedControl;
import com.skytree.epub.SearchListener;
import com.skytree.epub.SearchResult;

import java.util.ArrayList;

public class MagazineSearchActivity extends Activity implements View.OnClickListener {

    private Context context;

    private ImageButton btnDeleteText, btnClose;
    private EditText editTextSearch;
    private RecyclerView recyclerViewSearchResult;
    private LinearLayout searchInfo;

    private InputMethodManager imm;

    private FixedControl fv;

    private ArrayList<SearchResult> searchResults;
    private MagazinSearchResultAdapter searchResultAdapter;

    private DialogOneButton dialogOneButton;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

        setContentView(R.layout.activity_magazine_search);

        CommonUtil.makeFullscreen(this);

        context = this;

        fv = MagazineFixedControl.getInstance(this);
        fv.setSearchListener(new SearchDelegate());

        initView();

        searchResults = new ArrayList<>();

        imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);


        dialogOneButton = new DialogOneButton(this, getResources().getString(R.string.search_dialog_info), getResources().getString(R.string.dialog_confirm));

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnDeleteText :
                editTextSearch.setText("");
                editTextSearch.setCursorVisible(true);
                break;

            case R.id.btnClose :
                finish();
                overridePendingTransition(R.anim.stay, R.anim.sliding_down);
                break;
        }
    }

    class SearchDelegate implements SearchListener {

        public void onKeySearched(SearchResult searchResult) {
            //addSearchResult(searchResult,0);
            /*debug("chapterIndex"+searchResult.chapterIndex+" pageIndex:" + searchResult.pageIndex + " startOffset:"
                    + searchResult.startOffset + " tag:" + searchResult.nodeName
                    + " pagePositionInChapter "+searchResult.pagePositionInChapter+ " pagePositionInBook "+searchResult.pagePositionInBook + " text:" + searchResult.text);
             */

            Log.d("Activity", "SH Test - onKeySearched");
            searchResults.add(searchResult);
            searchResultAdapter.notifyDataSetChanged();

        }

        public void onSearchFinishedForChapter(SearchResult searchResult) {
            if (searchResult.numberOfSearchedInChapter!=0) {
               // addSearchResult(searchResult,1);
               // debug("Searching for Chapter:"+searchResult.chapterIndex+" is finished. ");
               // fv.pauseSearch();
               // numberOfSearched = searchResult.numberOfSearched;
                fv.searchMore();
                Log.d("Activity", "SH Test - !=0  searchMore ");

            }else {
                fv.searchMore();
               // numberOfSearched = searchResult.numberOfSearched;
                Log.d("Activity", "SH Test - searchMore");



            }

        }

        public void onSearchFinished(SearchResult searchResult) {
           // debug("Searching is finished. ");
           // addSearchResult(searchResult,2);
           // hideIndicator();
            Log.d("Activity", "SH Test - onSearchFinished");


        }
    }

    private void initView() {
        searchInfo = findViewById(R.id.searchInfo);

        editTextSearch = findViewById(R.id.editTextSearch);
        //editTextSearch.setImeOptions(EditorInfo.IME_ACTION_SEARCH);

        editTextSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((EditText) v).setCursorVisible(true);
            }
        });


        btnDeleteText = findViewById(R.id.btnDeleteText);
        btnDeleteText.setOnClickListener(this);

        btnClose = findViewById(R.id.btnClose);
        btnClose.setOnClickListener(this);

        recyclerViewSearchResult = findViewById(R.id.recyclerViewSearchResult);
        recyclerViewSearchResult.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));

        editTextSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                switch (actionId) {
                    case EditorInfo.IME_ACTION_SEARCH:
                        String key = editTextSearch.getText().toString();
                        if(key != null && key.length() > 1) {
                            fv.searchKey(key);
                            recyclerViewSearchResult.setVisibility(View.VISIBLE);
                            searchInfo.setVisibility(View.GONE);
                            imm.hideSoftInputFromWindow(editTextSearch.getWindowToken(), 0);

                            searchResultAdapter = new MagazinSearchResultAdapter(context, searchResults, key);
                            recyclerViewSearchResult.setAdapter(searchResultAdapter);

                            editTextSearch.setCursorVisible(false);
                        } else {
                            dialogOneButton.show();
                        }
                        // 검색 동작
                        break;
                    default:
                        // 기본 엔터키 동작
                        return false;
                }
                return true;
            }
        });

        editTextSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(count < 1) {
                    btnDeleteText.setVisibility(View.GONE);
                } else {
                    btnDeleteText.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }
}
