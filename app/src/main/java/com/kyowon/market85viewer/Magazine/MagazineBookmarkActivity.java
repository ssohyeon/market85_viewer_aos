package com.kyowon.market85viewer.Magazine;

import android.app.Activity;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.kyowon.market85viewer.BookInfo;
import com.kyowon.market85viewer.CommonUtil;
import com.kyowon.market85viewer.Constants;
import com.kyowon.market85viewer.DialogListener;
import com.kyowon.market85viewer.DialogTwoButton;
import com.kyowon.market85viewer.R;
import com.kyowon.market85viewer.RestFul.API.BookMarkDeleteAPI;
import com.kyowon.market85viewer.RestFul.API.BookMarkListAPI;
import com.kyowon.market85viewer.RestFul.item.BookMarkListItem;
import com.kyowon.market85viewer.RestFul.request.RequestBookMarkDelete;
import com.kyowon.market85viewer.RestFul.request.RequestBookMarkList;
import com.kyowon.market85viewer.RestFul.response.BaseResponse;
import com.kyowon.market85viewer.RestFul.response.ResponseBookMarkList;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MagazineBookmarkActivity extends Activity {

    private ImageButton btnCloseContentsList;
    private RecyclerView bookmarkRecyclerView;
    private LinearLayout viewBookmarkInfo;

    private ArrayList<BookMarkListItem> bookMarkItems = new ArrayList<>();

    private BookmarkAdapter bookmarkAdapter;

    private DialogTwoButton dialogTwoButton;

    // 다이얼로그 호출 시 소프트 키 안보이도록
    private View decorView;
    private int	uiOption;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

        setContentView(R.layout.activity_magazine_bookmark);

        CommonUtil.makeFullscreen(this);

        callAPI_bookmarkList();

        initView();

        //5페이지 ㅣ 2020.04.20
        bookmarkAdapter = new BookmarkAdapter(this, bookMarkItems);
        bookmarkRecyclerView.setAdapter(bookmarkAdapter);

        // 다이얼로그 호출 시 소프트 키 안보이도록
        decorView = getWindow().getDecorView();
        uiOption = getWindow().getDecorView().getSystemUiVisibility();
        if( Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH )
            uiOption |= View.SYSTEM_UI_FLAG_HIDE_NAVIGATION;
        if( Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN )
            uiOption |= View.SYSTEM_UI_FLAG_FULLSCREEN;
        if( Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT )
            uiOption |= View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;
    }

    public void initView() {
        btnCloseContentsList = findViewById(R.id.btnCloseContentsList);
        btnCloseContentsList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                overridePendingTransition(R.anim.stay, R.anim.sliding_down);
            }
        });

        bookmarkRecyclerView = findViewById(R.id.bookmarkRecyclerView);
        viewBookmarkInfo = findViewById(R.id.viewBookmarkInfo);


        bookmarkRecyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
    }



    public class BookmarkAdapter extends RecyclerView.Adapter<BookmarkAdapter.BookmarkViewHolder> {

        private Context context;

        private ArrayList<BookMarkListItem> bookMarkListItems;

        public BookmarkAdapter(Context context, ArrayList<BookMarkListItem> listItems) {
            this.context = context;
            bookMarkListItems = listItems;
        }


        @NonNull
        @Override
        public BookmarkViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(context).inflate(R.layout.bookmark_list_item, parent, false);
            BookmarkViewHolder viewHolder = new BookmarkViewHolder(view);

            return viewHolder;
        }

        @Override
        public void onBindViewHolder(@NonNull BookmarkViewHolder holder, final int position) {
            holder.txtBookmarkContent.setText(bookMarkItems.get(position).getTitle());

            String page = bookMarkListItems.get(position).getPage() + getResources().getString(R.string.bookmark_page);

            String[] create = bookMarkListItems.get(position).getCreated().split(" ");
            String date = create[0].replace("-", ".");


            String bookmarkInfo = page + getResources().getString(R.string.bookmark_divider) + date;

            holder.txtBookmarkInfo.setText(bookmarkInfo);

            holder.btnDeleteBookmark.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String message = context.getResources().getString(R.string.dialog_delete_bookmark_message);
                    String negative = context.getResources().getString(R.string.dialog_cancel);
                    String positive = context.getResources().getString(R.string.dialog_confirm);

                    dialogTwoButton = new DialogTwoButton(context, message, negative, positive);

                    dialogTwoButton.setDialogListener(new DialogListener() {
                        @Override
                        public void onNegativeClicked() {

                        }

                        @Override
                        public void onPositiveClicked() {


                            callAPI_bookMarkDelete(bookMarkItems.get(position).getSubscriptionBookmarkNo(), position);
                        }

                        @Override
                        public void onClose() {

                        }
                    });

                    dialogTwoButton.show();
                }
            });
        }

        @Override
        public int getItemCount() {
            int count = 0;

            if(bookMarkItems!=null)
                count = bookMarkItems.size();

            return count;
        }


        public class BookmarkViewHolder extends RecyclerView.ViewHolder {

            TextView txtBookmarkContent, txtBookmarkInfo;
            ImageButton btnDeleteBookmark;

            public BookmarkViewHolder(@NonNull View itemView) {
                super(itemView);

                txtBookmarkContent = itemView.findViewById(R.id.txtBookmarkContent);
                txtBookmarkInfo = itemView.findViewById(R.id.txtBookmarkInfo);

                btnDeleteBookmark = itemView.findViewById(R.id.btnDeleteBookmark);

            }
        }
    }


    public void callAPI_bookmarkList() {
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();

        httpClient.addInterceptor(new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                Request original = chain.request();

                Request request = original.newBuilder()
                        .header(Constants.HEADER_KEY, BookInfo.getCookie())
                        .method(original.method(), original.body())
                        .build();

                return chain.proceed(request);
            }
        });

        OkHttpClient client = httpClient.build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();

        BookMarkListAPI bookMarkListAPI = retrofit.create(BookMarkListAPI.class);

        RequestBookMarkList bookMarkList = new RequestBookMarkList();
        bookMarkList.set_siteId(Constants.SITE_ID);
        bookMarkList.setContentId(BookInfo.getContentsId());
        bookMarkList.setPage(1);
        bookMarkList.setPageSize(10);

        Call<ResponseBookMarkList> call = bookMarkListAPI.getPost(bookMarkList.get_siteId(), bookMarkList.getContentId(),
                bookMarkList.getPage(), bookMarkList.getPageSize());

        call.enqueue(new Callback<ResponseBookMarkList>() {
            @Override
            public void onResponse(Call<ResponseBookMarkList> call, Response<ResponseBookMarkList> response) {
                if (response.body() == null) {
                    return;
                }
                Log.d("BookmarkActivity", "SH Test - BookMarkListAPI response : " + response.toString());
                Log.d("BookmarkActivity", "SH Test - getResult " + response.body().getResult());
                Log.d("BookmarkActivity", "SH Test - getResultMessage " + response.body().getResultMessage());

                if(response.body().getResult().equals("401")) {
                    String message = "API 호출 - " + response.body().getResult() + " 쿠키값 변경";
                    Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();

                    viewBookmarkInfo.setVisibility(View.VISIBLE);
                    bookmarkRecyclerView.setVisibility(View.GONE);
                }

                ResponseBookMarkList responseBookMarkList = response.body();

                if(responseBookMarkList.getItems() != null) {

                    for (int i = 0; i < responseBookMarkList.getItems().size(); i++) {
                        BookMarkListItem item = responseBookMarkList.getItems().get(i);
                        bookMarkItems.add(item);

                        bookmarkAdapter.notifyDataSetChanged();
                    }

                    if(bookMarkItems==null || bookMarkItems.size()==0) {
                        viewBookmarkInfo.setVisibility(View.VISIBLE);
                        bookmarkRecyclerView.setVisibility(View.GONE);
                    } else {
                        viewBookmarkInfo.setVisibility(View.GONE);
                        bookmarkRecyclerView.setVisibility(View.VISIBLE);
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBookMarkList> call, Throwable t) {
                Log.d("BookmarkActivity", "SH Test - BookMarkListAPI onFailure : " + t.getMessage());
            }
        });
    }

    public void callAPI_bookMarkDelete(int subscriptionBookmarkNo, final int position) {
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();

        httpClient.addInterceptor(new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                Request original = chain.request();

                Request request = original.newBuilder()
                        .header(Constants.HEADER_KEY, BookInfo.getCookie())
                        .method(original.method(), original.body())
                        .build();

                return chain.proceed(request);
            }
        });

        OkHttpClient client = httpClient.build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();

        BookMarkDeleteAPI bookMarkDeleteAPI = retrofit.create(BookMarkDeleteAPI.class);

        RequestBookMarkDelete bookMarkDelete = new RequestBookMarkDelete();
        bookMarkDelete.set_siteId(Constants.SITE_ID);
        bookMarkDelete.setSubscriptionBookmarkNo(subscriptionBookmarkNo);

        //Call<BaseResponse> call = bookMarkDeleteAPI.post_posts(bookMarkDelete);
        Call<BaseResponse> call = bookMarkDeleteAPI.post_posts(bookMarkDelete.get_siteId(), bookMarkDelete.getSubscriptionBookmarkNo());

        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                if (response.body() == null) {
                    return;
                }
                Log.d("BookmarkActivity",  "SH Test - response : " + response.toString());
                Log.d("BookmarkActivity", "SH Test - getResult " + response.body().getResult());
                Log.d("BookmarkActivity", "SH Test - getResultMessage " + response.body().getResultMessage());

                if(response.body().getResult().equals("401")) {
                    String message = "API 호출 - " + response.body().getResult() + " 쿠키값 변경";
                    Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();

                }


                if(response.isSuccessful()) {
                    bookMarkItems.remove(position);
                    bookmarkAdapter.notifyDataSetChanged();

                    if(bookMarkItems.size() == 0 ){
                        viewBookmarkInfo.setVisibility(View.VISIBLE);
                        bookmarkRecyclerView.setVisibility(View.GONE);
                    }
                }
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {

            }
        });

    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        // TODO Auto-generated method stub
        // super.onWindowFocusChanged(hasFocus);

        if( hasFocus ) {
            decorView.setSystemUiVisibility( uiOption );
        }
    }
}
