package com.kyowon.market85viewer.Magazine;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.graphics.Rect;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.kyowon.market85viewer.R;

import java.io.File;
import java.util.List;

public class ThumbnailAdapter extends RecyclerView.Adapter<ThumbnailAdapter.ThumbnailViewHolder> {

    private Context context;
    private List<String> list;

    private int selectedItemPosition = -1;

    public ThumbnailAdapter(Context context, List<String> list) {
        this.context = context;
        this.list = list;
    }

    @NonNull
    @Override
    public ThumbnailViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.thumbnail_item, parent, false);
        return new ThumbnailViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ThumbnailViewHolder holder, int position) {

        String filePath = list.get(position);
        Bitmap bitmap = holder.getBitmap(filePath);
        holder.thumbnailView.setImageBitmap(bitmap);
        if (selectedItemPosition == position) {
            holder.setSelected(true);
        } else {
            holder.setSelected(false);
        }
    }


    @Override
    public int getItemCount() {
        int count = 0;

        if(list != null)
            count = list.size();

        return count;
    }

    public void selectItem(int selectedItemPosition) {
        this.selectedItemPosition = selectedItemPosition;
    }


    public class ThumbnailViewHolder extends RecyclerView.ViewHolder {

        ImageView thumbnailView;

        public ThumbnailViewHolder(@NonNull View itemView) {
            super(itemView);

            thumbnailView = (ImageView)itemView.findViewById(R.id.imgSelectedThumbnail);
        }

        public Bitmap getBitmap(String filePath) {
            Bitmap bitmap = null;
            try {
                File file = new File(filePath);
                if (file.exists()) {
                    bitmap = BitmapFactory.decodeFile(filePath);
                }
            }catch(Exception e) {
                e.printStackTrace();
            }
            return bitmap;
        }

        public void setSelected(Boolean isSelected) {
            if (isSelected) {
                thumbnailView.setBackgroundResource(R.color.bgSelectedThumbnailItem);
                this.itemView.setScaleX(1.f);
                this.itemView.setScaleY(1.f);
            } else {
                thumbnailView.setBackgroundResource(R.color.transparent);
                this.itemView.setScaleX(0.9f);
                this.itemView.setScaleY(0.9f);
            }
        }
    }
}
