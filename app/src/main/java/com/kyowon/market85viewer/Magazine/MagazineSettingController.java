package com.kyowon.market85viewer.Magazine;

import android.content.Context;

import com.kyowon.market85viewer.Constants;
import com.kyowon.market85viewer.PreferenceManager;

public class MagazineSettingController {

    private Context context;

    public MagazineSettingController(Context context) {
        this.context = context;
    }


    // 가로 모드에서 양면보기
    public boolean getDoublePage() {
        return PreferenceManager.getBoolean(context, Constants.PREFERENCES_DOUBLE_PAGE, true);
    }

    public void setDoublePage(boolean value) {
        PreferenceManager.setBoolean(context, Constants.PREFERENCES_DOUBLE_PAGE, value);
    }

    // 책장 넘김 효과
    public boolean getCurlEffect() {
        return PreferenceManager.getBoolean(context, Constants.PREFERENCES_CURL_EFFECT, true);
    }

    public void setCurlEffect(boolean value) {
        PreferenceManager.setBoolean(context, Constants.PREFERENCES_CURL_EFFECT, value);
    }

    // 화면 자동 잠금 방지
    public boolean getKeepScreenOn() {
        return PreferenceManager.getBoolean(context, Constants.PREFERENCES_KEEP_SCREEN_ON, true);
    }

    public void setKeepScreenOn(boolean value) {
        PreferenceManager.setBoolean(context, Constants.PREFERENCES_KEEP_SCREEN_ON, value);
    }

    // 화면 회정 고정
    public boolean getLockRotation() {
        return PreferenceManager.getBoolean(context, Constants.PREFERENCES_LOCK_ROTATION, true);
    }

    public void setLockRotation(boolean value) {
        PreferenceManager.setBoolean(context, Constants.PREFERENCES_LOCK_ROTATION, value);
    }

    // 상하 제스쳐로 밝기 조절
    public boolean getBrightnessControl() {
        return PreferenceManager.getBoolean(context, Constants.PREFERENCES_BRIGHTNESS_CONTROL, false);
    }

    public void setBrightnessControl(boolean value) {
        PreferenceManager.setBoolean(context, Constants.PREFERENCES_BRIGHTNESS_CONTROL, value);
    }

    // 오디오 북 자동 재생
    public boolean getAutoPlay() {
        return PreferenceManager.getBoolean(context, Constants.PREFERENCES_AUTO_PLAY, true);
    }

    public void setAutoPlay(boolean value) {
        PreferenceManager.setBoolean(context, Constants.PREFERENCES_AUTO_PLAY, value);
    }


}
