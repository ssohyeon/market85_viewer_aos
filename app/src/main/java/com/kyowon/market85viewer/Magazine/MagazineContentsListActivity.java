package com.kyowon.market85viewer.Magazine;

import android.app.Activity;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.kyowon.market85viewer.CommonUtil;
import com.kyowon.market85viewer.R;
import com.skytree.epub.FixedControl;
import com.skytree.epub.NavPoint;
import com.skytree.epub.NavPoints;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class MagazineContentsListActivity extends Activity {

    private static final String TAG = "MagazineContentsListActivity";

    private FixedControl fv;
    private NavPoints nps;

    private List<String> thumbnailList;

    private RecyclerView contentsRecyclerView;
    private ImageButton btnCloseContentsList;
    private int spanCount;

    private int thumbnailIndex = -1;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

        setContentView(R.layout.activity_magazine_contents_list);

        CommonUtil.makeFullscreen(this);

        btnCloseContentsList = findViewById(R.id.btnCloseContentsList);
        btnCloseContentsList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                overridePendingTransition(R.anim.stay, R.anim.sliding_down);
            }
        });

        thumbnailList = getIntent().getStringArrayListExtra("thumbnailList");

        fv = MagazineFixedControl.getInstance(this);
        nps = fv.getNavPoints();

        contentsRecyclerView = findViewById(R.id.contentsRecyclerView);
        contentsRecyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));

        ContentsAdapter contentsAdapter = new ContentsAdapter(this, fv, thumbnailList);
        contentsRecyclerView.setAdapter(contentsAdapter);

        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = pxToDp(this, size.x);

        spanCount = (width - 32) / 100;

    }


    @Override
    protected void onResume() {
        super.onResume();
    }

    public class ContentsAdapter extends RecyclerView.Adapter<ContentsAdapter.ContentsViewHolder> {
        Context context;
        List<String> list;
        FixedControl fv;

        public ContentsAdapter (Context context, FixedControl fv, List<String> list) {
            this.context = context;
            this.fv = fv;
            this.list = list;
        }

        @NonNull
        @Override
        public ContentsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(context).inflate(R.layout.contents_list_item, parent, false);
            ContentsViewHolder viewHolder = new ContentsViewHolder(view);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(@NonNull ContentsViewHolder holder, int position) {
            Log.d("Activity", "SH Test - position : " + position);
            ArrayList<ThumbnailInfo> contentsThumbnailList = new ArrayList<>();

            if(fv.getNavPoints() != null) {
                NavPoint np = fv.getNavPoints().getNavPoint(position);
                String title = np.text;
                int page = np.chapterIndex + 1;
                String titlePage = page + "p";

                holder.title.setText(title);
                holder.titlePage.setText(titlePage);

                int startPage = 0;
                int endPage = 0;

                int lastContents = getItemCount()-1;

                if(position < lastContents) {
                    startPage = nps.getNavPoint(position).chapterIndex;
                    endPage = nps.getNavPoint(position+1).chapterIndex;
                } else {
                    startPage = nps.getNavPoint(position).chapterIndex;
                    endPage = fv.getPageCount();
                }
                Log.d("Activity", "SH Test - startPage : " + startPage);
                Log.d("Activity", "SH Test - endPage : " + endPage);

                for(int i=startPage; i<endPage; i++) {
                    ThumbnailInfo thumbnailInfo = new ThumbnailInfo();

                    thumbnailInfo.filePath = list.get(i);
                    thumbnailInfo.index = i;
                    contentsThumbnailList.add(thumbnailInfo);
                }
            }

            holder.subTitleView.setVisibility(View.GONE);

            ThumbnailAdapter thumbnailAdapter = new ThumbnailAdapter(context, contentsThumbnailList, fv.currentPageIndex);
            holder.thumbnailRecyclerView.setLayoutManager(new GridLayoutManager(context, spanCount, RecyclerView.VERTICAL, false));

            holder.thumbnailRecyclerView.setAdapter(thumbnailAdapter);
        }

        @Override
        public int getItemCount() {
            int count = 0;

            if(fv.getNavPoints() != null)
                count = fv.getNavPoints().getSize();

            return count;
        }

        public class ContentsViewHolder extends RecyclerView.ViewHolder {
            RelativeLayout titleView, subTitleView;

            TextView title, titlePage, subTitle, subTitlePage;
            RecyclerView thumbnailRecyclerView;

            public ContentsViewHolder(@NonNull View itemView) {
                super(itemView);
                titleView = itemView.findViewById(R.id.titleView);
                subTitleView = itemView.findViewById(R.id.subTitleView);

                title = itemView.findViewById(R.id.title);
                titlePage = itemView.findViewById(R.id.titlePage);
                subTitle = itemView.findViewById(R.id.subTitle);
                subTitlePage = itemView.findViewById(R.id.subTitlePage);

                thumbnailRecyclerView = itemView.findViewById(R.id.thumbnailRecyclerView);

                itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(fv.getNavPoints() != null) {
                            NavPoint np = fv.getNavPoints().getNavPoint(getLayoutPosition());

                            int page = np.chapterIndex;
                            fv.gotoPage(page);
                        }
                        ((Activity) context).finish();


                                // fv.gotoPage(contentsThumbnailList.get(getLayoutPosition()).getIndex());
                        ((Activity) context).overridePendingTransition(R.anim.stay, R.anim.sliding_down);
                    }
                });

                /*
                itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        ((Activity) context).finish();
                        fv.gotoPage(contentsThumbnailList.get(getAdapterPosition()).getIndex());
                        ((Activity) context).overridePendingTransition(R.anim.stay, R.anim.sliding_down);
                    }
                });
                 */
            }
        }
    }




    public class ThumbnailAdapter extends RecyclerView.Adapter<ThumbnailAdapter.ThumbnailViewHolder> {

        private Context context;

        private List<ThumbnailInfo> contentsThumbnailList;
        private int currentPageIndex;

        public ThumbnailAdapter(Context context, List<ThumbnailInfo> contentsThumbnailList, int currentPageIndex) {
            this.context = context;
            this.contentsThumbnailList = contentsThumbnailList;
            this.currentPageIndex = currentPageIndex;
        }

        @NonNull
        @Override
        public ThumbnailAdapter.ThumbnailViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(context).inflate(R.layout.contents_thumbnail_item, parent, false);

            ThumbnailAdapter.ThumbnailViewHolder viewHolder = new ThumbnailAdapter.ThumbnailViewHolder(view);

            return viewHolder;
        }

        @Override
        public void onBindViewHolder(@NonNull ThumbnailViewHolder holder, int position) {
            Bitmap bitmap = holder.getBitmap(contentsThumbnailList.get(position).getFilePath());

            if(thumbnailIndex == currentPageIndex) {
                holder.thumbnailView.setVisibility(View.GONE);
                holder.selectedThumbnailView.setVisibility(View.VISIBLE);
                holder.selectedThumbnailView.setTag(thumbnailIndex);
                holder.selectedThumbnailView.setImageBitmap(bitmap);
            } else {
                holder.thumbnailView.setTag(thumbnailIndex);
                holder.thumbnailView.setImageBitmap(bitmap);
            }

            thumbnailIndex++;
        }

        @Override
        public int getItemCount() {
            int count = 0;

            if(contentsThumbnailList != null)
                count = contentsThumbnailList.size();

            return count;
        }


        public class ThumbnailViewHolder extends RecyclerView.ViewHolder {

            ImageView selectedThumbnailView;
            ImageView thumbnailView;

            public ThumbnailViewHolder(@NonNull final View itemView) {
                super(itemView);

                selectedThumbnailView = itemView.findViewById(R.id.imgSelectedThumbnail);
                thumbnailView = itemView.findViewById(R.id.imgThumbnail);

                itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        ((Activity) context).finish();
                        fv.gotoPage(contentsThumbnailList.get(getLayoutPosition()).getIndex());
                        ((Activity) context).overridePendingTransition(R.anim.stay, R.anim.sliding_down);
                    }
                });
            }

            public Bitmap getBitmap(String filePath) {
                Bitmap bitmap = null;
                try {
                    File file = new File(filePath);
                    if (file.exists()) {
                        bitmap = BitmapFactory.decodeFile(filePath);
                    }
                }catch(Exception e) {
                    e.printStackTrace();
                }
                return bitmap;
            }
        }
    }

    private float dpToPx(Context context, float dp) {
        DisplayMetrics dm = context.getResources().getDisplayMetrics();

        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, dm);
    }

    private int pxToDp(Context context, int px) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        return Math.round(px / (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
    }

    public class ThumbnailInfo {
        String filePath;
        int index;

        public String getFilePath() {
            return filePath;
        }

        public void setFilePath(String filePath) {
            this.filePath = filePath;
        }

        public int getIndex() {
            return index;
        }

        public void setIndex(int index) {
            this.index = index;
        }
    }

}
