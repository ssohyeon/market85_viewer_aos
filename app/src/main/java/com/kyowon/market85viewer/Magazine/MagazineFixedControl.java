package com.kyowon.market85viewer.Magazine;

import android.content.Context;

import com.skytree.epub.FixedControl;

public class MagazineFixedControl {

    private static FixedControl fixedControl;

    public static FixedControl getInstance(Context context) {
        if(fixedControl == null) {
          fixedControl = new FixedControl(context, 0, 0, 2);
        }

        return fixedControl;
    }

}
