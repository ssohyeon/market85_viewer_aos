package com.kyowon.market85viewer.Magazine;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.GestureDetector;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.Group;
import androidx.core.view.GestureDetectorCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager2.widget.CompositePageTransformer;
import androidx.viewpager2.widget.ViewPager2;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import com.kyowon.market85viewer.BookInfo;
import com.kyowon.market85viewer.BookPageInfo;
import com.kyowon.market85viewer.BookmarkInfo;
import com.kyowon.market85viewer.CommonUtil;
import com.kyowon.market85viewer.Constants;
import com.kyowon.market85viewer.DialogAutoPlayInfo;
import com.kyowon.market85viewer.DialogListener;
import com.kyowon.market85viewer.FileManager;
import com.kyowon.market85viewer.OnSingleClickListener;
import com.kyowon.market85viewer.PreferenceManager;
import com.kyowon.market85viewer.R;
import com.kyowon.market85viewer.RestFul.API.BookMarkAPI;
import com.kyowon.market85viewer.RestFul.API.BookMarkDeleteAPI;
import com.kyowon.market85viewer.RestFul.API.BookMarkListAPI;
import com.kyowon.market85viewer.RestFul.API.RecordAPI;
import com.kyowon.market85viewer.RestFul.item.BookMarkListItem;
import com.kyowon.market85viewer.RestFul.request.RequestBookMark;
import com.kyowon.market85viewer.RestFul.request.RequestBookMarkDelete;
import com.kyowon.market85viewer.RestFul.request.RequestBookMarkList;
import com.kyowon.market85viewer.RestFul.request.RequestRecord;
import com.kyowon.market85viewer.RestFul.response.BaseResponse;
import com.kyowon.market85viewer.RestFul.response.ResponseBookMark;
import com.kyowon.market85viewer.RestFul.response.ResponseBookMarkList;
import com.skytree.epub.Book;
import com.skytree.epub.BookInformation;
import com.skytree.epub.CacheListener;
import com.skytree.epub.ClickListener;
import com.skytree.epub.FixedControl;
import com.skytree.epub.KeyListener;
import com.skytree.epub.MediaOverlayListener;
import com.skytree.epub.NavPoint;
import com.skytree.epub.NavPoints;
import com.skytree.epub.PageInformation;
import com.skytree.epub.PageMovedListener;
import com.skytree.epub.PageTransition;
import com.skytree.epub.Parallel;
import com.skytree.epub.SkyProvider;
import com.skytree.epub.SkyUtterance;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;
import static com.kyowon.market85viewer.CommonUtil.dpToPx;

public class MagazineActivity extends Activity implements View.OnClickListener, SeekBar.OnSeekBarChangeListener {
    private static final String TAG = "MagazineActivity";

    private int screenType = -1;

    private RelativeLayout epubView;
    private FixedControl fv;
    private BookInformation bi;

    private RelativeLayout topViewGroup, bottomViewGroup;
    private LinearLayout mediaControllerGroup, audioAlertGroup;
    private Group mediaSpeedGroup;

    private TextView txtTitle, pageInfo;
    private ImageButton btnBack, btnSearch, btnContents, btnMediaOverlay, btnBookmark, btnSetting;
    private ImageButton btnSpeedSubtraction, btnSpeed, btnMediaPrev, btnMediaPlay, btnMediaNext, btnMediaAudio;
    private View mediaControllerDivider;
    private SeekBar seekBarPageInfo;
    private ImageView swipingHelpView;
    private ImageButton btnBookmarkLeft, btnBookmarkRight;

    private DialogAutoPlayInfo dialogAutoPlayInfo;

    private Parallel currentParallel;

    private PreferenceManager preferenceManager;
    private MagazineUIController magazineUIController;
    private MagazineMediaController magazineMediaController;
    private MagazineSettingController magazineSettingController;

    private ArrayList<String> thumbnailList;
    private ThumbnailAdapter adapter;
    private Thumbnail2Adapter adapter2;

    private FileManager fileManager;
    private RecyclerView thumbnailRecyclerView;
    private ViewPager2 viewPagerThumbnail;

    private int currentLeftPage = 0, currentRightPage = 0;

    boolean isInitialized = false;

    private View touchView;

    private ArrayList<BookPageInfo> pageInfoList;
    private ArrayList<BookmarkInfo> bookmarkInfoList;

    // 다이얼로그 호출 시 소프트 키 안보이도록
    private View decorView;
    private int	uiOption;

    private String title;

    private long mLastClickTime = 0;

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

        fileManager = new FileManager(this);

        preferenceManager = new PreferenceManager();
        magazineMediaController = new MagazineMediaController(this);
        magazineSettingController = new MagazineSettingController(this);

        setContentView(R.layout.activity_magazine);
        CommonUtil.makeFullscreen(this);

        title = getIntent().getStringExtra("title");

        preferenceManager.setBoolean(this, Constants.PREFERENCES_MAGAZINE_FIRST_OPEN, true);

        // 다이얼로그 호출 시 소프트 키 안보이도록
        decorView = getWindow().getDecorView();
        uiOption = getWindow().getDecorView().getSystemUiVisibility();
        if( Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH )
            uiOption |= View.SYSTEM_UI_FLAG_HIDE_NAVIGATION;
        if( Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN )
            uiOption |= View.SYSTEM_UI_FLAG_FULLSCREEN;
        if( Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT )
            uiOption |= View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;

        // 화면 자동 잠금 방지
        if(preferenceManager.getBoolean(this, Constants.PREFERENCES_KEEP_SCREEN_ON, true)) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        }

        // TODO : 화면 회전 고정 처리 다시 해야됨
        if(preferenceManager.getBoolean(this, Constants.PREFERENCES_LOCK_ROTATION, true)) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        }

        thumbnailList = new ArrayList<>();

        initFixedControl();
        initView();

        if(!preferenceManager.getBoolean(this, Constants.PREFERENCES_AUTO_PLAY_INFO, false)) {
            dialogAutoPlayInfo = new DialogAutoPlayInfo(this);
            dialogAutoPlayInfo.getWindow().setGravity(Gravity.TOP);
            WindowManager.LayoutParams params = dialogAutoPlayInfo.getWindow().getAttributes();

            params.y = (int) dpToPx(this, -50);
            dialogAutoPlayInfo.getWindow().setAttributes(params);

            dialogAutoPlayInfo.getWindow().setAttributes(params);
            dialogAutoPlayInfo.setDialogListener(new DialogListener() {
                @Override
                public void onNegativeClicked() {

                }

                @Override
                public void onPositiveClicked() {

                }

                @Override
                public void onClose() {
                    initializeIfBookLoaded();
                }
            });
        } else {
            initializeIfBookLoaded();
        }

        touchView = findViewById(R.id.touchView);
        touchView.setOnTouchListener(new BrightnessGestureListener(MagazineActivity.this));
        touchView.setVisibility(GONE);

        // TODO : bookmark 관련 기능 정리 필요
        pageInfoList = new ArrayList<>();

    }

    @Override
    protected void onResume() {
        super.onResume();

        if (fv.isDoublePaged()) {
            Log.d(TAG, "SH Test - isDoublePaged");
        }

        if(magazineSettingController.getCurlEffect()) {
            fv.setPageTransition(PageTransition.Curl);
        }
        else {
            fv.setPageTransition(PageTransition.None);
        }

        fv.invalidate();

        magazineMediaController.mediaPlayResume();
    }

    @Override
    protected void onPause() {
        super.onPause();

        magazineMediaController.mediaPlayPause();
    }



    private void initView() {

        topViewGroup = findViewById(R.id.topViewGroup);
        bottomViewGroup = findViewById(R.id.bottomViewGroup);
        mediaControllerGroup = findViewById(R.id.mediaControllerGroup);
        mediaSpeedGroup = findViewById(R.id.mediaSpeedGroup);
        audioAlertGroup = findViewById(R.id.audioAlertGroup);

        // 뷰어 클릭 방지
        topViewGroup.setOnClickListener(this);
        bottomViewGroup.setOnClickListener(this);

        topViewGroup.setVisibility(VISIBLE);
        bottomViewGroup.setVisibility(VISIBLE);

        btnBack = findViewById(R.id.btnBack);
        btnSearch = findViewById(R.id.btnSearch);
        btnContents = findViewById(R.id.btnContents);
        btnSetting = findViewById(R.id.btnSetting);
        btnBookmark = findViewById(R.id.btnBookmark);

        txtTitle = findViewById(R.id.txtTitle);
        txtTitle.setText(title);

        btnBookmarkLeft = findViewById(R.id.btnBookmarkLeft);
        btnBookmarkLeft.setOnClickListener(this);

        btnBookmarkRight = findViewById(R.id.btnBookmarkRight);
        btnBookmarkRight.setOnClickListener(this);

        btnMediaOverlay = findViewById(R.id.btnMediaOverlay);

        btnMediaPlay = findViewById(R.id.btnMediaPlay);
        btnMediaPlay.setOnClickListener(this);

        btnMediaPrev = findViewById(R.id.btnMediaPrev);
        btnMediaPrev.setOnTouchListener(new AudioControllerTouchListener());


        btnMediaNext = findViewById(R.id.btnMediaNext);
        btnMediaNext.setOnTouchListener(new AudioControllerTouchListener());

        btnSpeed = findViewById(R.id.btnSpeed);
        //btnSpeed.setOnTouchListener(new AudioControllerTouchListener());

        btnMediaAudio = findViewById(R.id.btnMediaAudio);
        btnMediaAudio.setOnTouchListener(new AudioControllerTouchListener());

        mediaControllerDivider = findViewById(R.id.mediaControllerDivider);

        seekBarPageInfo = findViewById(R.id.seekBarPageInfo);
        seekBarPageInfo.setOnSeekBarChangeListener(this);

        swipingHelpView = findViewById(R.id.imgViewAnimation);


        thumbnailRecyclerView = findViewById(R.id.thumbnailRecyclerView);
        //thumbnailRecyclerView.setVisibility(GONE);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this) {
            @Override
            public boolean canScrollHorizontally() {
                return false;
            }
        };
        layoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);


        thumbnailRecyclerView.addItemDecoration(new SpacesItemDecoration(this, thumbnailList));

        thumbnailRecyclerView.setLayoutManager(layoutManager);

        pageInfo = findViewById(R.id.pageInfo);

        viewPagerThumbnail = findViewById(R.id.viewPagerThumbnail);
        viewPagerThumbnail.setOrientation(ViewPager2.ORIENTATION_HORIZONTAL);
        viewPagerThumbnail.setClipToPadding(false);
        viewPagerThumbnail.setClipChildren(false);
        viewPagerThumbnail.setOffscreenPageLimit(10);
        //viewPagerThumbnail.setPadding(1000, 0,1000,0 );


        DisplayMetrics displayMetrics = getApplicationContext().getResources().getDisplayMetrics();

        int screenWidth = displayMetrics.widthPixels;
        float itemWidth = 0;


        CompositePageTransformer compositePageTransformer = new CompositePageTransformer();
        compositePageTransformer.addTransformer(new ViewPager2.PageTransformer() {
            @Override
            public void transformPage(@NonNull View page, float position) {


                Log.d(TAG, "SH Test - viewPagerThumbnail transformPage : " + position);
                if(position == 0) {
                    page.setScaleX(1.0f);
                    page.setScaleY(1.0f);
                    //viewPagerThumbnail.setScaleX(1.5f);
                    //viewPagerThumbnail.setScaleY(1.5f);
                    page.setBackgroundColor(getResources().getColor(R.color.epubTopView));
                } else {
                    page.setScaleX(0.9f);
                    page.setScaleY(0.9f);
                    //viewPagerThumbnail.setScaleX(0.5f);
                    //viewPagerThumbnail.setScaleY(0.5f);\
                    page.setBackgroundColor(getResources().getColor(R.color.transparent));
                }
            }
        });
        viewPagerThumbnail.setPageTransformer(compositePageTransformer);

        viewPagerThumbnail.registerOnPageChangeCallback(new ViewPager2.OnPageChangeCallback() {
            @Override
            public void onPageSelected(int position) {
                super.onPageSelected(position);

                Log.d(TAG, "SH Test - viewPagerThumbnail onPageSelected : " + position);

                Test test = new Test(getApplicationContext(), thumbnailList, position);
                viewPagerThumbnail.setPadding(test.padding()+50, 50, test.padding()+50, 50);

            }
        });

        setBtnOnClickListener();

    }

    private class Test {
        private Context context;
        private List<String> list;
        private int position;

        public Test (Context context, List<String> list, float position) {
            this.context = context;
            this.list = list;
            this.position = (int) position;
        }

        public int padding() {
            Bitmap bitmap = getBitmap(list.get(position));
            float itemWidth = 0;
            if (bitmap != null) {
                float bitmapWidth = bitmap.getWidth();
                float bitmapHeight = bitmap.getHeight();
                float scaleRatio = (int) dpToPx(context, 88) / bitmapHeight;
                itemWidth = bitmapWidth * scaleRatio;
            }

            DisplayMetrics displayMetrics = getApplicationContext().getResources().getDisplayMetrics();
            int screenWidth = displayMetrics.widthPixels;

            return (int)(screenWidth / 2 - itemWidth / 2);
        }


        public Bitmap getBitmap(String filePath) {
            Bitmap bitmap = null;
            try {
                File file = new File(filePath);
                if (file.exists()) {
                    bitmap = BitmapFactory.decodeFile(filePath);
                }
            }catch(Exception e) {
                e.printStackTrace();
            }
            return bitmap;
        }
    }

    private class SpacesItemDecoration extends RecyclerView.ItemDecoration {
        private Context context;
        private List<String> list;

        public SpacesItemDecoration(Context context, List<String> list) {
            this.context =context;
            this.list = list;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            int position = parent.getChildAdapterPosition(view);
            boolean isLast = position == state.getItemCount()-1;

            DisplayMetrics displayMetrics = getApplicationContext().getResources().getDisplayMetrics();

            int screenWidth = displayMetrics.widthPixels;

            Bitmap bitmap = getBitmap(list.get(position));
            float itemWidth = 0;
            if (bitmap != null) {
                float bitmapWidth = bitmap.getWidth();
                float bitmapHeight = bitmap.getHeight();
                float scaleRatio = (int) dpToPx(context, 80) / bitmapHeight;
                itemWidth = bitmapWidth * scaleRatio + (int) dpToPx(context, 8);
            }


            if (position == 0) {
                outRect.left = (int)(screenWidth / 2 - itemWidth / 2);
                outRect.right = (int) dpToPx(context, 10);
            } else if (isLast) {
                outRect.left = (int)dpToPx(context, 10);
                outRect.right = (int)(screenWidth / 2 - itemWidth / 2);
            } else {
                outRect.left = (int)dpToPx(context, 10);
                outRect.right = (int)dpToPx(context, 10);
            }
        }

        public Bitmap getBitmap(String filePath) {
            Bitmap bitmap = null;
            try {
                File file = new File(filePath);
                if (file.exists()) {
                    bitmap = BitmapFactory.decodeFile(filePath);
                }
            }catch(Exception e) {
                e.printStackTrace();
            }
            return bitmap;
        }
    }


    public void initFixedControl() {

        epubView = findViewById(R.id.epubView);

        fv = MagazineFixedControl.getInstance(this); //= new FixedControl(this, 0, 0, 2);

        String bookPath = fileManager.bookFilePath(BookInfo.getContentsId());

        fv.setBookPath(bookPath);

        SkyProvider skyProvider = new SkyProvider();
        skyProvider.setKeyListener(new KeyDelegate());
        fv.setContentProvider(skyProvider);
        SkyProvider skyProviderForCache = new SkyProvider();
        skyProviderForCache.setKeyListener(new KeyDelegate());
        fv.setContentProviderForCache(skyProviderForCache);

        getBookInformation();

        String script = "function beep() {"+
                "var sound = new Audio('data:audio/wav;base64,//uQRAAAAWMSLwUIYAAsYkXgoQwAEaYLWfkWgAI0wWs/ItAAAGDgYtAgAyN+QWaAAihwMWm4G8QQRDiMcCBcH3Cc+CDv/7xA4Tvh9Rz/y8QADBwMWgQAZG/ILNAARQ4GLTcDeIIIhxGOBAuD7hOfBB3/94gcJ3w+o5/5eIAIAAAVwWgQAVQ2ORaIQwEMAJiDg95G4nQL7mQVWI6GwRcfsZAcsKkJvxgxEjzFUgfHoSQ9Qq7KNwqHwuB13MA4a1q/DmBrHgPcmjiGoh//EwC5nGPEmS4RcfkVKOhJf+WOgoxJclFz3kgn//dBA+ya1GhurNn8zb//9NNutNuhz31f////9vt///z+IdAEAAAK4LQIAKobHItEIYCGAExBwe8jcToF9zIKrEdDYIuP2MgOWFSE34wYiR5iqQPj0JIeoVdlG4VD4XA67mAcNa1fhzA1jwHuTRxDUQ//iYBczjHiTJcIuPyKlHQkv/LHQUYkuSi57yQT//uggfZNajQ3Vmz+Zt//+mm3Wm3Q576v////+32///5/EOgAAADVghQAAAAA//uQZAUAB1WI0PZugAAAAAoQwAAAEk3nRd2qAAAAACiDgAAAAAAABCqEEQRLCgwpBGMlJkIz8jKhGvj4k6jzRnqasNKIeoh5gI7BJaC1A1AoNBjJgbyApVS4IDlZgDU5WUAxEKDNmmALHzZp0Fkz1FMTmGFl1FMEyodIavcCAUHDWrKAIA4aa2oCgILEBupZgHvAhEBcZ6joQBxS76AgccrFlczBvKLC0QI2cBoCFvfTDAo7eoOQInqDPBtvrDEZBNYN5xwNwxQRfw8ZQ5wQVLvO8OYU+mHvFLlDh05Mdg7BT6YrRPpCBznMB2r//xKJjyyOh+cImr2/4doscwD6neZjuZR4AgAABYAAAABy1xcdQtxYBYYZdifkUDgzzXaXn98Z0oi9ILU5mBjFANmRwlVJ3/6jYDAmxaiDG3/6xjQQCCKkRb/6kg/wW+kSJ5//rLobkLSiKmqP/0ikJuDaSaSf/6JiLYLEYnW/+kXg1WRVJL/9EmQ1YZIsv/6Qzwy5qk7/+tEU0nkls3/zIUMPKNX/6yZLf+kFgAfgGyLFAUwY//uQZAUABcd5UiNPVXAAAApAAAAAE0VZQKw9ISAAACgAAAAAVQIygIElVrFkBS+Jhi+EAuu+lKAkYUEIsmEAEoMeDmCETMvfSHTGkF5RWH7kz/ESHWPAq/kcCRhqBtMdokPdM7vil7RG98A2sc7zO6ZvTdM7pmOUAZTnJW+NXxqmd41dqJ6mLTXxrPpnV8avaIf5SvL7pndPvPpndJR9Kuu8fePvuiuhorgWjp7Mf/PRjxcFCPDkW31srioCExivv9lcwKEaHsf/7ow2Fl1T/9RkXgEhYElAoCLFtMArxwivDJJ+bR1HTKJdlEoTELCIqgEwVGSQ+hIm0NbK8WXcTEI0UPoa2NbG4y2K00JEWbZavJXkYaqo9CRHS55FcZTjKEk3NKoCYUnSQ0rWxrZbFKbKIhOKPZe1cJKzZSaQrIyULHDZmV5K4xySsDRKWOruanGtjLJXFEmwaIbDLX0hIPBUQPVFVkQkDoUNfSoDgQGKPekoxeGzA4DUvnn4bxzcZrtJyipKfPNy5w+9lnXwgqsiyHNeSVpemw4bWb9psYeq//uQZBoABQt4yMVxYAIAAAkQoAAAHvYpL5m6AAgAACXDAAAAD59jblTirQe9upFsmZbpMudy7Lz1X1DYsxOOSWpfPqNX2WqktK0DMvuGwlbNj44TleLPQ+Gsfb+GOWOKJoIrWb3cIMeeON6lz2umTqMXV8Mj30yWPpjoSa9ujK8SyeJP5y5mOW1D6hvLepeveEAEDo0mgCRClOEgANv3B9a6fikgUSu/DmAMATrGx7nng5p5iimPNZsfQLYB2sDLIkzRKZOHGAaUyDcpFBSLG9MCQALgAIgQs2YunOszLSAyQYPVC2YdGGeHD2dTdJk1pAHGAWDjnkcLKFymS3RQZTInzySoBwMG0QueC3gMsCEYxUqlrcxK6k1LQQcsmyYeQPdC2YfuGPASCBkcVMQQqpVJshui1tkXQJQV0OXGAZMXSOEEBRirXbVRQW7ugq7IM7rPWSZyDlM3IuNEkxzCOJ0ny2ThNkyRai1b6ev//3dzNGzNb//4uAvHT5sURcZCFcuKLhOFs8mLAAEAt4UWAAIABAAAAAB4qbHo0tIjVkUU//uQZAwABfSFz3ZqQAAAAAngwAAAE1HjMp2qAAAAACZDgAAAD5UkTE1UgZEUExqYynN1qZvqIOREEFmBcJQkwdxiFtw0qEOkGYfRDifBui9MQg4QAHAqWtAWHoCxu1Yf4VfWLPIM2mHDFsbQEVGwyqQoQcwnfHeIkNt9YnkiaS1oizycqJrx4KOQjahZxWbcZgztj2c49nKmkId44S71j0c8eV9yDK6uPRzx5X18eDvjvQ6yKo9ZSS6l//8elePK/Lf//IInrOF/FvDoADYAGBMGb7FtErm5MXMlmPAJQVgWta7Zx2go+8xJ0UiCb8LHHdftWyLJE0QIAIsI+UbXu67dZMjmgDGCGl1H+vpF4NSDckSIkk7Vd+sxEhBQMRU8j/12UIRhzSaUdQ+rQU5kGeFxm+hb1oh6pWWmv3uvmReDl0UnvtapVaIzo1jZbf/pD6ElLqSX+rUmOQNpJFa/r+sa4e/pBlAABoAAAAA3CUgShLdGIxsY7AUABPRrgCABdDuQ5GC7DqPQCgbbJUAoRSUj+NIEig0YfyWUho1VBBBA//uQZB4ABZx5zfMakeAAAAmwAAAAF5F3P0w9GtAAACfAAAAAwLhMDmAYWMgVEG1U0FIGCBgXBXAtfMH10000EEEEEECUBYln03TTTdNBDZopopYvrTTdNa325mImNg3TTPV9q3pmY0xoO6bv3r00y+IDGid/9aaaZTGMuj9mpu9Mpio1dXrr5HERTZSmqU36A3CumzN/9Robv/Xx4v9ijkSRSNLQhAWumap82WRSBUqXStV/YcS+XVLnSS+WLDroqArFkMEsAS+eWmrUzrO0oEmE40RlMZ5+ODIkAyKAGUwZ3mVKmcamcJnMW26MRPgUw6j+LkhyHGVGYjSUUKNpuJUQoOIAyDvEyG8S5yfK6dhZc0Tx1KI/gviKL6qvvFs1+bWtaz58uUNnryq6kt5RzOCkPWlVqVX2a/EEBUdU1KrXLf40GoiiFXK///qpoiDXrOgqDR38JB0bw7SoL+ZB9o1RCkQjQ2CBYZKd/+VJxZRRZlqSkKiws0WFxUyCwsKiMy7hUVFhIaCrNQsKkTIsLivwKKigsj8XYlwt/WKi2N4d//uQRCSAAjURNIHpMZBGYiaQPSYyAAABLAAAAAAAACWAAAAApUF/Mg+0aohSIRobBAsMlO//Kk4soosy1JSFRYWaLC4qZBYWFRGZdwqKiwkNBVmoWFSJkWFxX4FFRQWR+LsS4W/rFRb/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////VEFHAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAU291bmRib3kuZGUAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAMjAwNGh0dHA6Ly93d3cuc291bmRib3kuZGUAAAAAAAAAACU=');"
                +"sound.play(); }";
        fv.setCustomScript(script);
        fv.setTimeForRendering(500);
        fv.setNavigationAreaWidthRatio(0.0f);

        fv.setCurlQuality(0.6f);
        fv.setTimeForRendering(500);
        //fv.setNavigationAreaWidthRatio(0.0f);

        fv.setImmersiveMode(true);

        fv.setLicenseKey("ae98-8a9a-fee4-4de4");
        fv.setRotationLocked(true);

        fv.setClickListener(new ClickDelegate());
        fv.setMediaOverlayListener(new MediaOverlayDelegate());
        fv.setPageMovedListener(new PageMovedDelegate());
        fv.setCacheListener(new CacheDelegate());

        fv.setStartPageIndex(1);

        if(magazineSettingController.getCurlEffect())
            fv.setPageTransition(PageTransition.Curl);
        else
            fv.setPageTransition(PageTransition.None);

        epubView.addView(fv);

    }
    private class KeyDelegate implements KeyListener {
        @Override
        public String getKeyForEncryptedData(String uuidForContent, String contentName, String uuidForEpub) {
            // TODO Auto-generated method stub
            return "test";
        }

        @Override
        public Book getBook() {
            // TODO Auto-generated method stub
            return fv.getBook();
        }
    }


    private void getBookInformation () {
        SkyProvider skyProvider = new SkyProvider();
        bi = new BookInformation();
        bi.setFileName(fileManager.bookFileName(BookInfo.getContentsId()));
        bi.setBaseDirectory(fileManager.bookDirectory(BookInfo.getContentsId()));
        bi.setCoverPath("");
        bi.setContentProvider(skyProvider);
        skyProvider.setBook(bi.getBook());

        bi.makeInformation();
    }

    private void initializeIfBookLoaded() {
        final Handler getDataHandler = new Handler();
        getDataHandler.postDelayed(new Runnable() {
                                       @Override
                                       public void run() {
                                           if(fv.getBook() == null || fv.getNavPoints().getSize()==0) {
                                               getDataHandler.postDelayed(this, 500);
                                           } else {
                                               viewerGuideAnimation();

                                               if(magazineMediaController.hasMediaOverlay()  && fv.isMediaOverlayAvailable()) {
                                                   mediaControllerAnimation();
                                               }

                                               // 북마크
                                               if(pageInfoList != null && pageInfoList.size()==0) {
                                                   String title = "";
                                                   int navIndex = 0;

                                                   for (int i = 0; i < fv.getPageCount(); i++) {
                                                       BookPageInfo bookPageInfo = new BookPageInfo();
                                                       bookPageInfo.setPage(i+1);
                                                       bookPageInfo.setBookmark(false);
                                                       if(fv.getNavPoints() != null) {
                                                           NavPoint np = fv.getNavPoints().getNavPoint(navIndex);
                                                           if(np.chapterIndex == i) {
                                                               title = np.text;

                                                               if(navIndex < fv.getNavPoints().getSize()-1)
                                                                   navIndex++;
                                                           }
                                                       }
                                                       bookPageInfo.setChapterTitle(title);
                                                       pageInfoList.add(i, bookPageInfo);
                                                   }
                                                   callAPI_bookmarkList();
                                               }

                                               setScreen(Constants.SCREEN_TYPE_DEFAULT);

                                               startCaching();

                                           }

                                       }
                                   }
                , 500);
    }

    public void startCaching() {
        fv.startCaching();
    }



    private void setBookmark(int layoutId) {
        int listIndex = -1;
        String title;
        switch(layoutId) {
            case R.id.btnBookmarkLeft:
                listIndex = currentLeftPage-1;
                if(pageInfoList.get(listIndex).isBookmark()) {
                    // 북마크 삭제
                    callAPI_bookMarkDelete(pageInfoList.get(listIndex).getBookmarkInfo().getSubscriptionBookmarkNo(), listIndex);
                    btnBookmarkLeft.setImageDrawable(getResources().getDrawable(R.drawable.btn_viewer_cm_bookmark_n));
                } else {
                    // 북마크 등록
                    title = pageInfoList.get(listIndex).getChapterTitle();
                    callAPI_bookmark(currentLeftPage, title);
                    btnBookmarkLeft.setImageDrawable(getResources().getDrawable(R.drawable.btn_viewer_cm_bookmark_s));
                }
                break;

            case R.id.btnBookmarkRight:
                listIndex = currentRightPage-1;
                if(pageInfoList.get(listIndex).isBookmark()) {
                    // 북마크 삭제
                    callAPI_bookMarkDelete(pageInfoList.get(listIndex).getBookmarkInfo().getSubscriptionBookmarkNo(), listIndex);
                    btnBookmarkRight.setImageDrawable(getResources().getDrawable(R.drawable.btn_viewer_cm_bookmark_n));
                } else {
                    // 북마크 등록
                    title = pageInfoList.get(listIndex).getChapterTitle();
                    callAPI_bookmark(currentRightPage, title);
                    btnBookmarkRight.setImageDrawable(getResources().getDrawable(R.drawable.btn_viewer_cm_bookmark_s));
                }
                break;
        }
    }

    private void setTopBottomViewAnimation(int visibility) {
        float foldY = dpToPx(this, 48);

        if(visibility == View.VISIBLE) {
            topViewGroup.animate().translationY(topViewGroup.getTranslationY() + foldY);
            bottomViewGroup.animate().translationY(bottomViewGroup.getTranslationY() - foldY);
        } else {
            topViewGroup.animate().translationY(topViewGroup.getTranslationY() - foldY);
            bottomViewGroup.animate().translationY(bottomViewGroup.getTranslationY() + foldY);
        }
    }

    private void showMediaControllerAnimation(boolean isAutoReverse) {
        float openX = dpToPx(this, 0);
        float closeX = dpToPx(this, 44 - 280 - 10);
        if (isAutoReverse) {
            AnimatorSet animatorSet = new AnimatorSet();
            ObjectAnimator showAnim = ObjectAnimator.ofFloat(mediaControllerGroup, "translationX", openX);
            showAnim.setDuration(0);
            ObjectAnimator hideAnim = ObjectAnimator.ofFloat(mediaControllerGroup, "translationX", closeX);
            hideAnim.setStartDelay(2000);
            hideAnim.setDuration(500);
            animatorSet.play(showAnim).before(hideAnim);
            animatorSet.play(hideAnim).after(showAnim);
            animatorSet.start();
        } else {
            ObjectAnimator showAnim = ObjectAnimator.ofFloat(mediaControllerGroup, "translationX", openX);
            showAnim.setDuration(500);
            showAnim.start();
        }
    }

    private void hideMediaControllerAnimation() {
        float closeX = dpToPx(this, 44 - 280 - 10);
        ObjectAnimator hideAnim = ObjectAnimator.ofFloat(mediaControllerGroup, "translationX", closeX);
        hideAnim.setDuration(500);
        hideAnim.start();
    }

    private boolean isMediaControllerOpened() {
        return mediaControllerGroup.getTranslationX() >= 0;
    }


    private void setBtnMediaPlay() {
        if(fv.isPlayingPaused()) {
            if(fv.isMediaOverlayAvailable()) {
                btnMediaPlay.setImageDrawable(getResources().getDrawable(R.drawable.btn_viewer_control_play_n));
                btnMediaPlay.setColorFilter(getResources().getColor(R.color.btnMediaPress));
            } else {
                btnMediaPlay.setImageDrawable(getResources().getDrawable(R.drawable.btn_viewer_control_play_n));
                btnMediaPlay.setColorFilter(getResources().getColor(R.color.btnMediaDisable));
            }
        } else {
            btnMediaPlay.setImageDrawable(getResources().getDrawable(R.drawable.btn_viewer_control_pause_n));
            btnMediaPlay.setColorFilter(getResources().getColor(R.color.btnMediaPress));
        }
    }


    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

        Log.d(TAG, "SH Test - onProgressChanged");
        if (!fromUser) {            Log.d(TAG, "SH Test - !fromUser");
            return;
        }

        setSeekBar(progress, fv.getPageCount());
        thumbnailRecyclerView.getAdapter().notifyDataSetChanged();
        viewPagerThumbnail.setCurrentItem(progress, false);
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {
        Log.d(TAG, "SH Test - onStartTrackingTouch");

        thumbnailRecyclerView.setVisibility(VISIBLE);
        viewPagerThumbnail.setVisibility(VISIBLE);

    }

    @Override
    public void onStopTrackingTouch(final SeekBar seekBar) {

        final int pageNumber = seekBar.getProgress();

        Log.d(TAG, "SH Test --- pageNumber : " + pageNumber);
        Log.d(TAG, "SH Test --- currentLeftPage : " + currentLeftPage);
        Log.d(TAG, "SH Test --- currentRightPage : " + currentRightPage);

        if(pageNumber == currentLeftPage-1 || pageNumber == currentRightPage-1) {
            thumbnailRecyclerView.setVisibility(GONE);
            viewPagerThumbnail.setVisibility(GONE);
        } else {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    thumbnailRecyclerView.setVisibility(GONE);
                    viewPagerThumbnail.setVisibility(GONE);
                }
            }, 800);

            fv.gotoPage(pageNumber);
        }

    }

    private class MediaOverlayDelegate implements MediaOverlayListener {

        @Override
        public void onParallelStarted(Parallel parallel) {
            final String hash = parallel.hash;
            final int pageIndex = parallel.pageIndex;

            //fv.changeElementColor("#FFFF00", parallel.hash, parallel.pageIndex);
            if(parallel != null) {
                fv.changeElementColor("#FFFF00", parallel.hash, parallel.pageIndex);
            }

            currentParallel = parallel;
            Log.d(TAG, "SH Test - currentParallel : currentParallel.pageIndex " + currentParallel.pageIndex);
            Log.d(TAG, "SH Test - currentParallel : currentParallel.parallelIndex " + currentParallel.parallelIndex);

            setBtnMediaPlay();
        }

        @Override
        public void onParallelEnded(Parallel parallel) {
            Log.d(TAG, "SH Test - onParallelEnded");
            fv.restoreElementColor();
        }

        @Override
        public void onParallelsEnded() {
            Log.d(TAG, "SH Test - onParallelsEnded");
            fv.restoreElementColor();

            //if(magazineSettingController.getAutoPlay())
                //fv.gotoNextPage();

            fv.gotoPage(currentRightPage+1);
        }

        @Override
        public String postProcessText(String s) {
            return null;
        }

        @Override
        public ArrayList makeParallelsForTTS(int i, String s) {
            return null;
        }

        @Override
        public void onSynthesisToFileRequested(SkyUtterance skyUtterance) {

        }
    }

    private class PageMovedDelegate implements PageMovedListener {

        @Override
        public void onPageMoved(PageInformation pageInformation) {
            callAPI_record(pageInformation.pageIndex);

            if(magazineMediaController.hasMediaOverlay()) {
                if(fv.isMediaOverlayAvailable() && magazineMediaController.mediaOverlayMode()==Constants.MEDIA_OVERLAY_MODE_ON) {
                    new Handler().postDelayed(new Runnable() {
                        public void run() {
                            fv.playFirstParallel();
                        }
                    }, 500);

                } else if(fv.isMediaOverlayAvailable() && magazineMediaController.mediaOverlayMode()==Constants.MEDIA_OVERLAY_MODE_OFF) {
                    Log.d(TAG, "SH Test - 듣기 OFF");
                }
            } else {
                btnMediaOverlay.setImageDrawable(getResources().getDrawable(R.drawable.btn_viewer_controller_audio_dis));
            }

            if (!isInitialized) {
                setThumbnailView();
                isInitialized = true;
            }

            int pageNumber = pageInformation.pageIndex + 1;
            int totalPage = fv.getPageCount();

            if (fv.isDoublePaged() && (pageNumber - 1) != (seekBarPageInfo.getProgress() - 1)) {
                setSeekBar(pageNumber - 1, totalPage);
            } else if (!fv.isDoublePaged()) {
                setSeekBar(pageNumber - 1, totalPage);
            }

            if(magazineMediaController.hasMediaOverlay()) {
                if(dialogAutoPlayInfo != null) {
                    dialogAutoPlayInfo.show();
                    dialogAutoPlayInfo = null;
                }
            }

            int page = pageInformation.pageIndex+1;

            // 북마크 버튼 설정
            if(fv.isDoublePaged() && pageInfoList.size()!=0) {
                if(fv.currentPageIndex < 0) {
                    currentLeftPage = -1;
                    currentRightPage = page;

                    if(pageInfoList.get(currentRightPage-1).isBookmark()) {
                        btnBookmarkRight.setImageDrawable(getResources().getDrawable(R.drawable.btn_viewer_cm_bookmark_s));
                    } else {
                        btnBookmarkRight.setImageDrawable(getResources().getDrawable(R.drawable.btn_viewer_cm_bookmark_n));
                    }
                    btnBookmarkRight.setVisibility(View.VISIBLE);
                    btnBookmarkLeft.setVisibility(GONE);

                } else if (fv.currentPageIndex == totalPage-1) {
                    currentLeftPage = page;
                    currentRightPage = -1;

                    if(pageInfoList.get(currentLeftPage-1).isBookmark()) {
                        btnBookmarkLeft.setImageDrawable(getResources().getDrawable(R.drawable.btn_viewer_cm_bookmark_s));
                    } else {
                        btnBookmarkLeft.setImageDrawable(getResources().getDrawable(R.drawable.btn_viewer_cm_bookmark_n));
                    }

                    btnBookmarkRight.setVisibility(GONE);
                    btnBookmarkLeft.setVisibility(View.VISIBLE);
                } else {
                    currentLeftPage = page;
                    currentRightPage = page + 1;

                    if(pageInfoList.get(currentLeftPage-1).isBookmark()) {
                        btnBookmarkLeft.setImageDrawable(getResources().getDrawable(R.drawable.btn_viewer_cm_bookmark_s));
                    } else {
                        btnBookmarkLeft.setImageDrawable(getResources().getDrawable(R.drawable.btn_viewer_cm_bookmark_n));
                    }

                    if(pageInfoList.get(currentRightPage-1).isBookmark()) {
                        btnBookmarkRight.setImageDrawable(getResources().getDrawable(R.drawable.btn_viewer_cm_bookmark_s));
                    } else {
                        btnBookmarkRight.setImageDrawable(getResources().getDrawable(R.drawable.btn_viewer_cm_bookmark_n));
                    }

                    btnBookmarkRight.setVisibility(View.VISIBLE);
                    btnBookmarkLeft.setVisibility(View.VISIBLE);
                }

            } else {
                // TODO : 세로모드
                //currentRightPage = pageInformation.pageIndex;
            }


            setAudioControllerBtn();
        }

        @Override
        public void onChapterLoaded(int i) {
            Log.d(TAG, "SH Test - onChapterLoaded");
        }

        @Override
        public void onFailedToMove(boolean b) {

        }
    }

    private class CacheDelegate implements CacheListener {

        @Override
        public void onCachingStarted(int pageIndex) {
            Log.d(TAG, "onCachingStarted : " + pageIndex );
        }

        @Override
        public void onCachingFinished(int pageIndex) {
            Log.d(TAG, "onCachingFinished");
        }

        @Override
        public void onCached(int pageIndex, Bitmap bitmap, double v) {
            Log.d(TAG, "onCached");
            final Bitmap targetBitmap = bitmap;

            fileManager.makeFileDirectory(fileManager.thumbnailDirectory(BookInfo.getContentsId()));

            String filePath = fileManager.thumbnailFilePath(BookInfo.getContentsId(), pageIndex);
            fileManager.saveBitmap(targetBitmap, filePath);
        }

        @Override
        public boolean cacheExist(int pageIndex) {
            File file = new File(fileManager.thumbnailFilePath(BookInfo.getContentsId(), pageIndex));
            if(file.exists()) {
                Log.d(TAG, "cacheExist index = " + pageIndex);
                return true;
            }

            return false;
        }

        @Override
        public Bitmap getCachedBitmap(int pageIndex) {
            Log.d(TAG, "getCachedBitmap");
            Bitmap bitmap = fileManager.getBitmap(pageIndex);
            return bitmap;
        }
    }



    private class ClickDelegate implements ClickListener {
        @Override
        public void onClick(int x,int y) {

            if (SystemClock.elapsedRealtime() - mLastClickTime < 500){
                return;
            }

            mLastClickTime = SystemClock.elapsedRealtime();

            switch (getScreenType()) {
                case Constants.SCREEN_TYPE_DEFAULT:
                    setScreen(Constants.SCREEN_TYPE_COMMON);
                    break;
                case Constants.SCREEN_TYPE_COMMON:
                    setScreen(Constants.SCREEN_TYPE_DEFAULT);
                    break;
            }
        }

        public void onImageClicked(int x,int y,String src) {}
        public void onLinkClicked(int x,int y,String href) {}
        @Override
        public boolean ignoreLink(int x,int y,String href) {
            // TODO Auto-generated method stub
            return false;
        }
        @Override
        public void onLinkForLinearNoClicked(int x, int y, String href) {}
        @Override
        public void onIFrameClicked(int x, int y, String src) {
            // TODO Auto-generated method stub

        }
        @Override
        public void onVideoClicked(int x, int y, String src) {
            // TODO Auto-generated method stub

        }
        @Override
        public void onAudioClicked(int x, int y, String src) {
            // TODO Auto-generated method stub

        }
    }

    private void setBookmarkButton(int visibility) {
        if(fv.isDoublePaged()) {
            btnBookmarkRight.setVisibility(visibility);
            btnBookmarkLeft.setVisibility(visibility);
        } else {
            btnBookmarkRight.setVisibility(visibility);
            btnBookmarkLeft.setVisibility(GONE);
        }
    }

    private void setScreen(int screenType) {
        setScreenType(screenType);

        switch (screenType) {
            case Constants.SCREEN_TYPE_DEFAULT :
                setDefaultModeView();
                break;

            case Constants.SCREEN_TYPE_COMMON :
                setCommonModeView();
                break;
        }

    }

    private void setScreenType(int screenType) {
        this.screenType = screenType;
    }

    private int getScreenType() {
        return screenType;
    }

    private void setDefaultModeView() {
        setTopBottomViewAnimation(View.GONE);
        setBookmarkButton(View.VISIBLE);

        if(magazineMediaController.hasMediaOverlay()) {
            if(magazineMediaController.mediaOverlayMode() == Constants.MEDIA_OVERLAY_MODE_OFF) {
                mediaControllerGroup.setVisibility(View.GONE);
                btnMediaOverlay.setImageDrawable(getResources().getDrawable(R.drawable.btn_viewer_controller_audio_on));
            } else {
                mediaControllerGroup.setVisibility(View.VISIBLE);
                btnMediaOverlay.setImageDrawable(getResources().getDrawable(R.drawable.btn_viewer_controller_audio_off));
                showMediaControllerAnimation(true);
            }
        } else {
            mediaControllerGroup.setVisibility(View.GONE);
            btnMediaOverlay.setImageDrawable(getResources().getDrawable(R.drawable.btn_viewer_controller_audio_dis));
        }
    }

    private void setCommonModeView() {
        setTopBottomViewAnimation(View.VISIBLE);
        setBookmarkButton(View.GONE);
        mediaControllerGroup.setVisibility(View.GONE);
    }

    private void setThumbnailView() {

        adapter = new ThumbnailAdapter(this, thumbnailList);
        adapter2 = new Thumbnail2Adapter(this, thumbnailList);

        for(int i=0; i<fv.getPageCount(); i++) {
            thumbnailList.add(fileManager.thumbnailFilePath(BookInfo.getContentsId(), i));
        }

        thumbnailRecyclerView.setAdapter(adapter2);
        viewPagerThumbnail.setAdapter(adapter);

        adapter.notifyDataSetChanged();
        adapter2.notifyDataSetChanged();
    }

    private void setSeekBar(int progress, int total) {
        Log.d(TAG, "SH Test - setSeekBar ");
        DisplayMetrics displayMetrics = getApplicationContext().getResources().getDisplayMetrics();
        int screenWidth = displayMetrics.widthPixels;

        seekBarPageInfo.setMax(total - 1);
        seekBarPageInfo.setProgress(progress);

        String strPageInfo = (progress + 1) + "/" + total;
        SpannableString spannableString = new SpannableString(strPageInfo);
        spannableString.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.white)), 0, strPageInfo.indexOf("/"), SpannableString.SPAN_EXCLUSIVE_EXCLUSIVE);

        pageInfo.setText(spannableString);

        if (thumbnailRecyclerView.findViewHolderForAdapterPosition(progress) != null) {
            Log.d(TAG, "SH Test - findViewHolderForAdapterPosition progress" + progress);
            //ThumbnailAdapter.ThumbnailViewHolder holder = (ThumbnailAdapter.ThumbnailViewHolder)thumbnailRecyclerView.findViewHolderForAdapterPosition(progress);
            //((ThumbnailAdapter)thumbnailRecyclerView.getAdapter()).selectItem(progress);
            Thumbnail2Adapter.ThumbnailViewHolder holder2 = (Thumbnail2Adapter.ThumbnailViewHolder)thumbnailRecyclerView.findViewHolderForAdapterPosition(progress);
            ((Thumbnail2Adapter)thumbnailRecyclerView.getAdapter()).selectItem(progress);
            thumbnailRecyclerView.getAdapter().notifyDataSetChanged();
            int itemWidth = holder2.itemView.getWidth();
            ((LinearLayoutManager)thumbnailRecyclerView.getLayoutManager()).scrollToPositionWithOffset(progress, screenWidth / 2 - itemWidth / 2 - (int)dpToPx(this, 10));
        }
    }

    private void viewerGuideAnimation() {
        if(preferenceManager.getBoolean(this, Constants.PREFERENCES_MAGAZINE_FIRST_OPEN, true)) {
            final float deltaX = dpToPx(this, 8);

            swipingHelpView.setVisibility(VISIBLE);

            AnimatorSet animatorSet = new AnimatorSet();

            ObjectAnimator shrinkAnim = ObjectAnimator.ofFloat(swipingHelpView, "translationX", swipingHelpView.getTranslationX() + deltaX);
            shrinkAnim.setDuration(500);
            shrinkAnim.setRepeatCount(5);
            shrinkAnim.setRepeatMode(ValueAnimator.REVERSE);

            ObjectAnimator hideAnim = ObjectAnimator.ofFloat(swipingHelpView, "translationX", swipingHelpView.getTranslationX() + swipingHelpView.getWidth());
            hideAnim.setDuration(300);
            hideAnim.setStartDelay(500);

            ObjectAnimator fadeOutAnim = ObjectAnimator.ofFloat(swipingHelpView, "alpha", 0);
            fadeOutAnim.setDuration(200);
            fadeOutAnim.setStartDelay(500);

            animatorSet.play(shrinkAnim).before(hideAnim);
            animatorSet.play(hideAnim).with(fadeOutAnim);
            animatorSet.start();

            preferenceManager.setBoolean(this, Constants.PREFERENCES_MAGAZINE_FIRST_OPEN, false);
        }
    }

    private void mediaControllerAnimation() {
        final float foldX = dpToPx(this, 244);
        mediaControllerGroup.setVisibility(VISIBLE);

        new Handler().postDelayed(new Runnable() {
            public void run() {
                mediaControllerGroup.animate().translationX(mediaControllerGroup.getTranslationX() - foldX);
            }
        }, 2000);
    }

    public void callAPI_record(int page) {
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();

        httpClient.addInterceptor(new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                Request original = chain.request();

                Request request = original.newBuilder()
                        .header(Constants.HEADER_KEY, BookInfo.getCookie())
                        .method(original.method(), original.body())
                        .build();

                return chain.proceed(request);
            }
        });

        OkHttpClient client = httpClient.build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();

        // 여기에 데이터 넣어주기.. header, 현재 이지, 등등
        RecordAPI recordAPI = retrofit.create(RecordAPI.class);

        RequestRecord item = new RequestRecord();
        item.set_siteId(Constants.SITE_ID);
        item.setContentsId(BookInfo.getContentsId());
        item.setRecord(page);

        Call <BaseResponse> call = recordAPI.getPost(item.get_siteId(), item.getContentsId(), item.getRecord());

        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                if(response.isSuccessful()) {
                    Log.d(TAG,  "SH Test - response : " + response.toString());
                    Log.d(TAG, "SH Test - getResult " + response.body().getResult());
                    Log.d(TAG, "SH Test - getResultMessage " + response.body().getResultMessage());
                }
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {

            }
        });
    }

    public void callAPI_bookmark(final int currentPage, String title) {
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();

        httpClient.addInterceptor(new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                Request original = chain.request();

                Request request = original.newBuilder()
                        .header(Constants.HEADER_KEY, BookInfo.getCookie())
                        .method(original.method(), original.body())
                        .build();

                return chain.proceed(request);
            }
        });

        OkHttpClient client = httpClient.build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();

        BookMarkAPI bookMarkAPI = retrofit.create(BookMarkAPI.class);

        RequestBookMark bookMark = new RequestBookMark();
        bookMark.set_siteId(Constants.SITE_ID);
        bookMark.setContentsId(BookInfo.getContentsId());
        bookMark.setPage(currentPage);
        bookMark.setTitle(title);

        Call<ResponseBookMark> call = bookMarkAPI.getPost(bookMark.get_siteId(), bookMark.getContentsId(), bookMark.getPage(), bookMark.getTitle());

        call.enqueue(new Callback<ResponseBookMark>() {
            @Override
            public void onResponse(Call<ResponseBookMark> call, Response<ResponseBookMark> response) {
                if (response.body() == null) {
                    return;
                }
                Log.d(TAG, "SH Test - BookMarkAPI response : " + response.toString());
                Log.d(TAG, "SH Test - getResult " + response.body().getResult());
                Log.d(TAG, "SH Test - getResultMessage " + response.body().getResultMessage());

                if(response.body().getResult().equals("401")) {
                    String message = "API 호출 - " + response.body().getResult() + " 쿠키값 변경";
                    Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                }

                ResponseBookMark responseBookMark = response.body();

                pageInfoList.get(currentPage-1).setBookmark(true);
            }

            @Override
            public void onFailure(Call<ResponseBookMark> call, Throwable t) {

            }
        });

    }

    public void callAPI_bookMarkDelete(int subscriptionBookmarkNo, final int position) {
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();

        httpClient.addInterceptor(new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                Request original = chain.request();

                Request request = original.newBuilder()
                        .header(Constants.HEADER_KEY, BookInfo.getCookie())
                        .method(original.method(), original.body())
                        .build();

                return chain.proceed(request);
            }
        });

        OkHttpClient client = httpClient.build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();

        BookMarkDeleteAPI bookMarkDeleteAPI = retrofit.create(BookMarkDeleteAPI.class);

        RequestBookMarkDelete bookMarkDelete = new RequestBookMarkDelete();
        bookMarkDelete.set_siteId(Constants.SITE_ID);
        bookMarkDelete.setSubscriptionBookmarkNo(subscriptionBookmarkNo);

        //Call<BaseResponse> call = bookMarkDeleteAPI.post_posts(bookMarkDelete);
        Call<BaseResponse> call = bookMarkDeleteAPI.post_posts(bookMarkDelete.get_siteId(), bookMarkDelete.getSubscriptionBookmarkNo());

        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                if (response.body() == null) {
                    return;
                }

                if(response.body().getResult().equals("401")) {
                    String message = "API 호출 - " + response.body().getResult() + " 쿠키값 변경";
                    Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                }

                Log.d("BookmarkActivity",  "SH Test - response : " + response.toString());
                Log.d("BookmarkActivity", "SH Test - getResult " + response.body().getResult());
                Log.d("BookmarkActivity", "SH Test - getResultMessage " + response.body().getResultMessage());

                if(response.isSuccessful()) {
                    pageInfoList.get(position).setBookmark(false);
                }
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {

            }
        });

    }

    public void callAPI_bookmarkList() {
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();

        httpClient.addInterceptor(new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                Request original = chain.request();

                Request request = original.newBuilder()
                        .header(Constants.HEADER_KEY, BookInfo.getCookie())
                        .method(original.method(), original.body())
                        .build();

                return chain.proceed(request);
            }
        });

        OkHttpClient client = httpClient.build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();

        BookMarkListAPI bookMarkListAPI = retrofit.create(BookMarkListAPI.class);

        RequestBookMarkList bookMarkList = new RequestBookMarkList();
        bookMarkList.set_siteId(Constants.SITE_ID);
        bookMarkList.setContentId(BookInfo.getContentsId());
        bookMarkList.setPage(1);
        bookMarkList.setPageSize(10);

        Call<ResponseBookMarkList> call = bookMarkListAPI.getPost(bookMarkList.get_siteId(), bookMarkList.getContentId(),
                bookMarkList.getPage(), bookMarkList.getPageSize());

        call.enqueue(new Callback<ResponseBookMarkList>() {
            @Override
            public void onResponse(Call<ResponseBookMarkList> call, Response<ResponseBookMarkList> response) {
                if (response.body() == null) {
                    return;
                }

                if(response.body().getResult().equals("401")) {
                    String message = "API 호출 - " + response.body().getResult() + " 쿠키값 변경";
                    Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                }

                Log.d(TAG, "SH Test - BookMarkListAPI response : " + response.toString());
                Log.d(TAG, "SH Test - getResult " + response.body().getResult());
                Log.d(TAG, "SH Test - getResultMessage " + response.body().getResultMessage());

                ResponseBookMarkList responseBookMarkList = response.body();

                if(responseBookMarkList.getItems() != null) {

                    for (int i = 0; i < responseBookMarkList.getItems().size(); i++) {
                        BookMarkListItem item = responseBookMarkList.getItems().get(i);
                        BookmarkInfo bookmarkInfo = new BookmarkInfo();
                        bookmarkInfo.setTitle(item.getTitle());
                        bookmarkInfo.setPage(item.getPage());
                        bookmarkInfo.setSubscriptionBookmarkNo(item.getSubscriptionBookmarkNo());

                        BookPageInfo bookPageInfo = new BookPageInfo();
                        bookPageInfo.setBookmark(true);
                        bookPageInfo.setPage(item.getPage());
                        bookPageInfo.setChapterTitle(item.getTitle());
                        bookPageInfo.setBookmarkInfo(bookmarkInfo);

                        pageInfoList.set(item.getPage()-1, bookPageInfo);
                    }

                }
            }

            @Override
            public void onFailure(Call<ResponseBookMarkList> call, Throwable t) {
                Log.d("BookmarkActivity", "SH Test - BookMarkListAPI onFailure : " + t.getMessage());
            }
        });
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        // TODO Auto-generated method stub
        // super.onWindowFocusChanged(hasFocus);

        if( hasFocus ) {
            decorView.setSystemUiVisibility( uiOption );
        }
    }

    public void setAudioControllerBtn() {
        if(!fv.isMediaOverlayAvailable()) {
            btnMediaPlay.setEnabled(false);
            //btnMediaPlay.setColorFilter(getResources().getColor(R.color.white));
            btnMediaPlay.setColorFilter(getResources().getColor(R.color.btnMediaDisable));

            //BlendModeColorFilter colorFilter = new BlendModeColorFilter(getResources().getColor(R.color.btnMediaDisable), BlendMode.DST_IN);

            btnMediaPrev.setEnabled(false);
            btnMediaPrev.setColorFilter(getResources().getColor(R.color.btnMediaDisable));

            btnMediaNext.setEnabled(false);
            btnMediaNext.setColorFilter(getResources().getColor(R.color.btnMediaDisable));
        } else {
            btnMediaPlay.setEnabled(true);

            btnMediaPrev.setEnabled(true);
            btnMediaPrev.setColorFilter(getResources().getColor(R.color.white));

            btnMediaNext.setEnabled(true);
            btnMediaNext.setColorFilter(getResources().getColor(R.color.white));
        }

    }

    public void setBtnOnClickListener() {
        btnMediaOverlay.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                int mediaOverlayMode = magazineMediaController.mediaOverlayMode();
                boolean hasMediaOverlay = magazineMediaController.hasMediaOverlay();

                if(hasMediaOverlay) {
                    if (mediaOverlayMode == Constants.MEDIA_OVERLAY_MODE_OFF) {
                        magazineMediaController.mediaOverlayPlayStart();
                    } else if (mediaOverlayMode == Constants.MEDIA_OVERLAY_MODE_ON) {
                        magazineMediaController.mediaOverlayPlayStop();
                    } else if (mediaOverlayMode == Constants.MEDIA_OVERLAY_MODE_DISABLE) {
                        Log.d(TAG, "SH Test - disable");
                    }
                }else {
                    audioAlertGroup.setVisibility(View.VISIBLE);
                    new Handler().postDelayed(new Runnable() {
                        public void run() {
                            audioAlertGroup.setVisibility(GONE);
                        }
                    }, 2000);
                }
                setScreen(Constants.SCREEN_TYPE_DEFAULT);
                setBtnMediaPlay();
                setAudioControllerBtn();
            }
        });

        btnSearch.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                Intent searchIntent = new Intent(MagazineActivity.this, MagazineSearchActivity.class);
                startActivity(searchIntent);
                overridePendingTransition(R.anim.sliding_up, R.anim.stay);
            }
        });

        btnContents.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                NavPoints nps = fv.getNavPoints();
                for (int i=0; i<nps.getSize(); i++) {
                    NavPoint np = nps.getNavPoint(i);
                }

                Intent contentsIntent = new Intent(MagazineActivity.this, MagazineContentsListActivity.class);
                contentsIntent.putExtra("thumbnailList", thumbnailList);
                startActivity(contentsIntent);
                overridePendingTransition(R.anim.sliding_up, R.anim.stay);
            }
        });

        btnSetting.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                Intent settingIntent = new Intent(MagazineActivity.this, MagazineSettingActivity.class);
                startActivity(settingIntent);
                overridePendingTransition(R.anim.sliding_up, R.anim.stay);
            }
        });

        btnBookmark.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                Intent bookmarkIntent = new Intent(MagazineActivity.this, MagazineBookmarkActivity.class);
                startActivity(bookmarkIntent);
                overridePendingTransition(R.anim.sliding_up, R.anim.stay);
            }
        });

        btnBack.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                finish();
            }
        });

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.btnMediaOverlay:

                break;

            case R.id.btnMediaPlay:
                magazineMediaController.setMediaPlayState();
                setBtnMediaPlay();
                break;

            case R.id.btnMediaPrev:
                Log.d(TAG, "SH Test - btnMediaPrev");
                magazineMediaController.mediaOverlayPlayPrev(currentParallel, magazineSettingController.getAutoPlay());
                break;

            case R.id.btnMediaNext:
                Log.d(TAG, "SH Test - btnMediaNext");
                magazineMediaController.mediaOverlayPlayNext();
                break;

            case R.id.btnMediaAudio :
                if (isMediaControllerOpened()) {
                    hideMediaControllerAnimation();
                } else {
                    showMediaControllerAnimation(false);
                }
                break;

            case R.id.btnBookmarkLeft:
            case R.id.btnBookmarkRight:
                setBookmark(v.getId());
                break;
        }
    }

    public class AudioControllerTouchListener implements View.OnTouchListener {

        @Override
        public boolean onTouch(View v, MotionEvent event) {
            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    if(v.getId() == R.id.btnMediaAudio) {
                        btnMediaAudio.setColorFilter(getResources().getColor(R.color.btnMediaPress));
                    } else if(v.getId() == R.id.btnMediaPrev) {
                        btnMediaPrev.setColorFilter(getResources().getColor(R.color.btnMediaPress));
                    } else if(v.getId() == R.id.btnMediaNext) {
                        btnMediaNext.setColorFilter(getResources().getColor(R.color.btnMediaPress));
                    } else if(v.getId() == R.id.btnSpeed) {
                        btnSpeed.setColorFilter(getResources().getColor(R.color.btnMediaPress));
                    }
                    break;

                case MotionEvent.ACTION_UP:
                    if(v.getId() == R.id.btnMediaAudio) {
                        btnMediaAudio.setColorFilter(getResources().getColor(R.color.white));

                        if (isMediaControllerOpened()) {
                            hideMediaControllerAnimation();
                        } else {
                            showMediaControllerAnimation(false);
                        }
                    } else if(v.getId() == R.id.btnMediaPrev) {
                        btnMediaPrev.setColorFilter(getResources().getColor(R.color.white));
                        magazineMediaController.mediaOverlayPlayPrev(currentParallel, magazineSettingController.getAutoPlay());
                    } else if(v.getId() == R.id.btnMediaNext) {
                        btnMediaNext.setColorFilter(getResources().getColor(R.color.white));
                        magazineMediaController.mediaOverlayPlayNext();
                    } else if(v.getId() == R.id.btnSpeed) {
                        btnSpeed.setColorFilter(getResources().getColor(R.color.white));
                    }
                    break;
            }

            return true;
        }


    }

}
