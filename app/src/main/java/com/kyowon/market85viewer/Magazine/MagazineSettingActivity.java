package com.kyowon.market85viewer.Magazine;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.Switch;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.kyowon.market85viewer.CommonUtil;
import com.kyowon.market85viewer.R;
import com.kyowon.market85viewer.TutorialActivity;
import com.skytree.epub.FixedControl;

public class MagazineSettingActivity extends Activity implements View.OnClickListener {

    private ImageButton btnCloseSetting, btnAutoPlayInfo, btnAutoPlayInfoClose;
    private TextView txtAutoPlayInfo, btnShowUserGuide;
    private View autoPlayInfoArrow;

    private Switch switchDoublePage, switchCurlEffect, switchKeepScreenOn, switchLockRotation, switchBrightnessControl, switchAutoPlay;

    private MagazineSettingController magazineSettingController;

    private FixedControl fv;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

        setContentView(R.layout.activity_magazine_setting);

        CommonUtil.makeFullscreen(this);

        fv = MagazineFixedControl.getInstance(this);

        initView();
        magazineSettingController = new MagazineSettingController(this);

        setSettingValue();

    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        Log.d("MagazineSettingActivity", "SH Test - onConfigurationChanged");
        super.onConfigurationChanged(newConfig);

        WindowManager wm = getWindowManager();
        if (wm == null)
            return;
        if(newConfig.orientation==newConfig.ORIENTATION_PORTRAIT){
            setContentView(R.layout.activity_magazine_setting);
        }else{
            setContentView(R.layout.activity_magazine_setting);
        }

        initView();
        setSettingValue();
    }



    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnCloseSetting:
                finish();
                overridePendingTransition(R.anim.stay, R.anim.sliding_down);
                break;
            case R.id.btnAutoPlayInfo:
                showInfo();
                break;

            case R.id.btnAutoPlayInfoClose:
                hideInfo();
                break;

            case R.id.btnShowUserGuide:
                Intent intent = new Intent(MagazineSettingActivity.this, TutorialActivity.class);
                startActivity(intent);
                break;
        }

    }

    private void showInfo() {
        btnAutoPlayInfoClose.setVisibility(View.VISIBLE);
        txtAutoPlayInfo.setVisibility(View.VISIBLE);
        autoPlayInfoArrow.setVisibility(View.VISIBLE);
    }

    private void hideInfo() {
        btnAutoPlayInfoClose.setVisibility(View.GONE);
        txtAutoPlayInfo.setVisibility(View.GONE);
        autoPlayInfoArrow.setVisibility(View.GONE);
    }

    private void initView() {
        btnCloseSetting = findViewById(R.id.btnCloseSetting);
        btnCloseSetting.setOnClickListener(this);

        btnAutoPlayInfo = findViewById(R.id.btnAutoPlayInfo);
        btnAutoPlayInfo.setOnClickListener(this);

        btnAutoPlayInfoClose = findViewById(R.id.btnAutoPlayInfoClose);
        btnAutoPlayInfoClose.setOnClickListener(this);

        txtAutoPlayInfo = findViewById(R.id.txtAutoPlayInfo);
        autoPlayInfoArrow = findViewById(R.id.autoPlayInfoArrow);

        switchDoublePage = findViewById(R.id.switchDoublePage);
        switchDoublePage.setOnCheckedChangeListener(new doublePageSwitchListener());

        switchCurlEffect = findViewById(R.id.switchCurlEffect);
        switchCurlEffect.setOnCheckedChangeListener(new curlEffectSwitchListener());

        switchKeepScreenOn = findViewById(R.id.switchKeepScreenOn);
        switchKeepScreenOn.setOnCheckedChangeListener(new keepScreenOnSwitchListener());

        switchLockRotation = findViewById(R.id.switchLockRotation);
        switchLockRotation.setOnCheckedChangeListener(new lockRotationSwitchListener());

        switchBrightnessControl = findViewById(R.id.switchBrightnessControl);
        switchBrightnessControl.setOnCheckedChangeListener(new brightnessControlSwitchListener());

        switchAutoPlay = findViewById(R.id.switchAutoPlay);
        switchAutoPlay.setOnCheckedChangeListener(new autoPlaySwitchListener());

        btnShowUserGuide = findViewById(R.id.btnShowUserGuide);
        btnShowUserGuide.setOnClickListener(this);
    }

    private void setSettingValue() {
        Log.d("MagazineSettingActivity", "SH Test - " + magazineSettingController.getDoublePage());
        switchDoublePage.setChecked(magazineSettingController.getDoublePage());
        switchCurlEffect.setChecked(magazineSettingController.getCurlEffect());
        switchKeepScreenOn.setChecked(magazineSettingController.getKeepScreenOn());
        switchLockRotation.setChecked(magazineSettingController.getLockRotation());
        switchBrightnessControl.setChecked(magazineSettingController.getBrightnessControl());
        switchAutoPlay.setChecked(magazineSettingController.getAutoPlay());
    }

    class doublePageSwitchListener implements Switch.OnCheckedChangeListener {

        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            magazineSettingController.setDoublePage(isChecked);
        }
    }

    class curlEffectSwitchListener implements Switch.OnCheckedChangeListener {

        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            magazineSettingController.setCurlEffect(isChecked);
            if(isChecked) {
                Log.d("Activity", "SH Test - PageTransition.Curl");
                //fv.setPageTransition(PageTransition.Curl);
            } else {
                Log.d("Activity", "SH Test - PageTransition.None");
                //fv.setPageTransition(PageTransition.None);
            }
        }
    }

    class keepScreenOnSwitchListener implements Switch.OnCheckedChangeListener {

        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            magazineSettingController.setKeepScreenOn(isChecked);
        }
    }

    class lockRotationSwitchListener implements Switch.OnCheckedChangeListener {

        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            magazineSettingController.setLockRotation(isChecked);
        }
    }

    class brightnessControlSwitchListener implements Switch.OnCheckedChangeListener {

        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            magazineSettingController.setBrightnessControl(isChecked);
        }
    }

    class autoPlaySwitchListener implements Switch.OnCheckedChangeListener {

        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            magazineSettingController.setAutoPlay(isChecked);
        }
    }
}
