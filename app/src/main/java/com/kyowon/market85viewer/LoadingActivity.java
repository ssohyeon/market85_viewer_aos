package com.kyowon.market85viewer;

import android.animation.Animator;
import android.animation.ValueAnimator;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.util.Pair;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.airbnb.lottie.LottieAnimationView;
import com.airbnb.lottie.LottieDrawable;
import com.kyowon.market85viewer.Magazine.MagazineActivity;
import com.kyowon.market85viewer.Magazine.MagazineFixedControl;
import com.kyowon.market85viewer.RestFul.API.Download;
import com.skytree.epub.Book;
import com.skytree.epub.BookInformation;
import com.skytree.epub.FixedControl;
import com.skytree.epub.KeyListener;
import com.skytree.epub.SkyProvider;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import okhttp3.OkHttpClient;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class LoadingActivity extends Activity {
    private static final String TAG = "LoadingActivity";

    public LottieAnimationView lottieAnimView1, lottieAnimView2, lottieAnimView3, lottieAnimView4;
    private TextView txtPercent;

    private FileManager fileManager;
    private DownloadZipFileTask downloadZipFileTask;

    private String contentsId;
    private int page;

    private PreferenceManager preferenceManaager;

    private boolean isTutorailShow = false;

    //private FixedControl fv;
    //private BookInformation bi;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

        setContentView(R.layout.activity_loading);

        CommonUtil.makeFullscreen(this);

        preferenceManaager = new PreferenceManager();
        isTutorailShow = preferenceManaager.getBoolean(this, Constants.PREFERENCES_TUTORIAL, false);

        initView();

        if(BookInfo.getContentsId()==null)
            BookInfo.setContentsId(getIntent().getStringExtra("contentsId"));

        page = getIntent().getIntExtra("page", 0);
        BookInfo.setCookie("kwopSvcJWT=SDP+eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJzdmMiLCJqdGkiOiJcYitcdTAwMUFcdTAwMUEpXHUwMDBFXHUwMDFFXHUwMDE2IiwiYXVkIjoiMTcyLjE2LjMxLjEzMSIsImlzcyI6IkktT04iLCJpYXQiOjE1OTIyNzE3MTksImV4cCI6MzI0NzIxMTE2MDB9.xqU2X4GuncrU0UFSGf_M9Iw3mCGVBi07W7ZX3ZbaZOlQin352liB1ecUH1IZ-ak4s8qAGvpcIIr4TS-Mi76WEQ");

        fileManager = new FileManager(this);

        String viewerDirectory = fileManager.viewerDirectory(BookInfo.getContentsId());
        fileManager.makeFileDirectory(viewerDirectory);

        String bookDirectory = fileManager.bookDirectory(BookInfo.getContentsId());
        fileManager.makeFileDirectory(bookDirectory);

        //fv = MagazineFixedControl.getInstance(this);
    }

    @Override
    protected void onResume() {
        super.onResume();

        if(fileManager.isBookSaved(BookInfo.getContentsId())){
            //setFixedControl();

            Log.d(TAG, "SH Test - fileManager.isBookSaved(contentsId)");
            finish();


            if(!isTutorailShow) {
                finish();
                Intent tutorialIntent = new Intent(LoadingActivity.this, TutorialActivity.class);
                tutorialIntent.putExtra("isFirst", true);
                tutorialIntent.putExtra("title", "");
                startActivity(tutorialIntent);
            } else {
                finish();
                Intent intent = new Intent(LoadingActivity.this, MagazineActivity.class);
                intent.putExtra("contentsId", BookInfo.getContentsId());
                intent.putExtra("page", page);
                intent.putExtra("title", "");
                startActivity(intent);
            }
        } else {
            Log.d(TAG, "SH Test - !fileManager.isBookSaved(contentsId)");
            downloadFile();
        }
    }

    private void downloadFile() {
        //https://cdn.allng.com/market85/Ethan_layout_pop-up.epub
        //http://scs.skyepub.net/samples/Doctor.epub
        Download downloadService = createService(Download.class, "https://cdn.allng.com/market85/");

        Call<ResponseBody> call = downloadService.downloadFileByUrl("Ethan_layout_pop-up.epub");

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, final Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    Log.d(TAG, "Got the body for the file");
                    downloadZipFileTask = new DownloadZipFileTask();
                    downloadZipFileTask.execute(response.body());
                } else {
                    Log.d(TAG, "Connection failed " + response.errorBody());
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                t.printStackTrace();
                Log.e(TAG, t.getMessage());
            }
        });
    }

    public <T> T createService(Class<T> serviceClass, String baseUrl) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(baseUrl)
                .client(new OkHttpClient.Builder().build())
                .build();
        return retrofit.create(serviceClass);
    }

    private class DownloadZipFileTask extends AsyncTask<ResponseBody, Pair<Integer, Long>, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected String doInBackground(ResponseBody... urls) {
            //Copy you logic to calculate progress and call
            {
                String filePath = fileManager.bookFilePath(BookInfo.getContentsId());

                Log.d(TAG, "SH Test - filePath : " + filePath);
                saveToDisk(urls[0], filePath);
            }
            return null;
        }

        protected void onProgressUpdate(Pair<Integer, Long>... progress) {

            Log.d("API123", progress[0].second + " ");

            //if (progress[0].first == 100)
                //Toast.makeText(getApplicationContext(), "File downloaded successfully", Toast.LENGTH_SHORT).show();


            if (progress[0].second > 0) {
                int currentProgress = (int) ((double) progress[0].first / (double) progress[0].second * 100);
                //progressBar.setProgress(currentProgress);

                txtPercent.setText(currentProgress + "%");

                setUpAnimation(currentProgress);

                if(currentProgress == 100) {
                    //setFixedControl();

                    if(!isTutorailShow) {
                        finish();
                        Intent tutorialIntent = new Intent(LoadingActivity.this, TutorialActivity.class);
                        tutorialIntent.putExtra("isFirst", true);
                        tutorialIntent.putExtra("title", "");
                        startActivity(tutorialIntent);
                    } else {
                        finish();
                        Intent startIntent = new Intent(LoadingActivity.this, MagazineActivity.class);
                        startIntent.putExtra("contentsId", BookInfo.getContentsId());
                        startIntent.putExtra("page", page);
                        startIntent.putExtra("title", "");
                        startActivity(startIntent);
                    }
                }
            }

            if (progress[0].first == -1) {
                Toast.makeText(getApplicationContext(), "Download failed", Toast.LENGTH_SHORT).show();
            }

        }

        public void doProgress(Pair<Integer, Long> progressDetails) {
            publishProgress(progressDetails);
        }

        @Override
        protected void onPostExecute(String result) {

        }
    }


    private void saveToDisk(ResponseBody body, String filePath) {

        try {
            InputStream inputStream = null;

            FileOutputStream localFileOutputStream =null;

            try {
                inputStream = body.byteStream();
                localFileOutputStream = new FileOutputStream(filePath);

                byte data[] = new byte[4096];
                int count;
                int progress = 0;
                long fileSize = body.contentLength();
                Log.d(TAG, "File Size=" + fileSize);
                while ((count = inputStream.read(data)) != -1) {
                    localFileOutputStream.write(data, 0, count);
                    //outputStream.write(data, 0, count);
                    progress += count;
                    Pair<Integer, Long> pairs = new Pair<>(progress, fileSize);
                    downloadZipFileTask.doProgress(pairs);
                    Log.d(TAG, "Progress: " + progress + "/" + fileSize + " >>>> " + (float) progress / fileSize);
                }

                localFileOutputStream.flush();
                //outputStream.flush();

                //Log.d(TAG, destinationFile.getParent());
                Pair<Integer, Long> pairs = new Pair<>(100, 100L);
                downloadZipFileTask.doProgress(pairs);
                return;
            } catch (IOException e) {
                e.printStackTrace();
                Pair<Integer, Long> pairs = new Pair<>(-1, Long.valueOf(-1));
                downloadZipFileTask.doProgress(pairs);
                Log.d(TAG, "Failed to save the file!");
                return;
            } finally {
                if (inputStream != null) inputStream.close();
                if (localFileOutputStream != null) localFileOutputStream.close();
                //if (outputStream != null) outputStream.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
            Log.d(TAG, "Failed to save the file!");
            return;
        }
    }

    private void setUpAnimation(int progress) {
        Log.d(TAG, "SH Test - setUpAnimation progress " + progress);
        if(progress==80) {
            lottieAnimView2.cancelAnimation();
            lottieAnimView2.setVisibility(View.GONE);
            lottieAnimView3.setVisibility(View.VISIBLE);

            lottieAnimView3.addAnimatorListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animation) {

                }

                @Override
                public void onAnimationEnd(Animator animation) {
                    lottieAnimView3.setVisibility(View.GONE);
                    lottieAnimView4.setVisibility(View.VISIBLE);
                }

                @Override
                public void onAnimationCancel(Animator animation) {
                    lottieAnimView3.setVisibility(View.GONE);
                    lottieAnimView4.setVisibility(View.VISIBLE);
                }

                @Override
                public void onAnimationRepeat(Animator animation) {

                }
            });

        } else if (progress==100) {
            if(lottieAnimView3.isAnimating()) {
                lottieAnimView3.cancelAnimation();
            }
        }
    }

    private void initView() {
        lottieAnimView1 = findViewById(R.id.lottieAnimView_1);
        lottieAnimView2 = findViewById(R.id.lottieAnimView_2);
        lottieAnimView3 = findViewById(R.id.lottieAnimView_3);
        lottieAnimView4 = findViewById(R.id.lottieAnimView_4);
        //setUpAnimation(lottieAnimView1);

        txtPercent = findViewById(R.id.txtPercent);

        lottieAnimView1.setAnimation("01_start.json");
        lottieAnimView2.setAnimation("02_middle.json");
        lottieAnimView3.setAnimation("03_middle_end.json");
        lottieAnimView4.setAnimation("04_end.json");

        lottieAnimView1.playAnimation();

        lottieAnimView2.setRepeatCount(LottieDrawable.INFINITE);
        lottieAnimView2.playAnimation();

        lottieAnimView3.playAnimation();
        lottieAnimView4.playAnimation();


        lottieAnimView1.addAnimatorListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {
                Log.d(TAG, "SH Test - lottieAnimView1 onAnimationStart");
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                Log.d(TAG, "SH Test - lottieAnimView1 onAnimationEnd");

                lottieAnimView1.setVisibility(View.GONE);
                lottieAnimView2.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationCancel(Animator animation) {
                Log.d(TAG, "SH Test - lottieAnimView1 onAnimationCancel");
            }

            @Override
            public void onAnimationRepeat(Animator animation) {
                Log.d(TAG, "SH Test - lottieAnimView1 onAnimationRepeat");
            }
        });
    }

    private void setUpAnimation(LottieAnimationView animationView) {
        animationView.setAnimation("Market85_Ebook_Loading.json");

        animationView.setRepeatCount(LottieDrawable.INFINITE);

        animationView.playAnimation();
        animationView.addAnimatorListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {

            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
    }

    /*private void setFixedControl() {
        String bookPath = fileManager.bookFilePath(BookInfo.getContentsId());

        fv.setBookPath(bookPath);

        SkyProvider skyProvider = new SkyProvider();
        skyProvider.setKeyListener(new KeyDelegate());
        fv.setContentProvider(skyProvider);
        SkyProvider skyProviderForCache = new SkyProvider();
        skyProviderForCache.setKeyListener(new KeyDelegate());
        fv.setContentProviderForCache(skyProviderForCache);

        getBookInformation();

        String script = "function beep() {"+
                "var sound = new Audio('data:audio/wav;base64,//uQRAAAAWMSLwUIYAAsYkXgoQwAEaYLWfkWgAI0wWs/ItAAAGDgYtAgAyN+QWaAAihwMWm4G8QQRDiMcCBcH3Cc+CDv/7xA4Tvh9Rz/y8QADBwMWgQAZG/ILNAARQ4GLTcDeIIIhxGOBAuD7hOfBB3/94gcJ3w+o5/5eIAIAAAVwWgQAVQ2ORaIQwEMAJiDg95G4nQL7mQVWI6GwRcfsZAcsKkJvxgxEjzFUgfHoSQ9Qq7KNwqHwuB13MA4a1q/DmBrHgPcmjiGoh//EwC5nGPEmS4RcfkVKOhJf+WOgoxJclFz3kgn//dBA+ya1GhurNn8zb//9NNutNuhz31f////9vt///z+IdAEAAAK4LQIAKobHItEIYCGAExBwe8jcToF9zIKrEdDYIuP2MgOWFSE34wYiR5iqQPj0JIeoVdlG4VD4XA67mAcNa1fhzA1jwHuTRxDUQ//iYBczjHiTJcIuPyKlHQkv/LHQUYkuSi57yQT//uggfZNajQ3Vmz+Zt//+mm3Wm3Q576v////+32///5/EOgAAADVghQAAAAA//uQZAUAB1WI0PZugAAAAAoQwAAAEk3nRd2qAAAAACiDgAAAAAAABCqEEQRLCgwpBGMlJkIz8jKhGvj4k6jzRnqasNKIeoh5gI7BJaC1A1AoNBjJgbyApVS4IDlZgDU5WUAxEKDNmmALHzZp0Fkz1FMTmGFl1FMEyodIavcCAUHDWrKAIA4aa2oCgILEBupZgHvAhEBcZ6joQBxS76AgccrFlczBvKLC0QI2cBoCFvfTDAo7eoOQInqDPBtvrDEZBNYN5xwNwxQRfw8ZQ5wQVLvO8OYU+mHvFLlDh05Mdg7BT6YrRPpCBznMB2r//xKJjyyOh+cImr2/4doscwD6neZjuZR4AgAABYAAAABy1xcdQtxYBYYZdifkUDgzzXaXn98Z0oi9ILU5mBjFANmRwlVJ3/6jYDAmxaiDG3/6xjQQCCKkRb/6kg/wW+kSJ5//rLobkLSiKmqP/0ikJuDaSaSf/6JiLYLEYnW/+kXg1WRVJL/9EmQ1YZIsv/6Qzwy5qk7/+tEU0nkls3/zIUMPKNX/6yZLf+kFgAfgGyLFAUwY//uQZAUABcd5UiNPVXAAAApAAAAAE0VZQKw9ISAAACgAAAAAVQIygIElVrFkBS+Jhi+EAuu+lKAkYUEIsmEAEoMeDmCETMvfSHTGkF5RWH7kz/ESHWPAq/kcCRhqBtMdokPdM7vil7RG98A2sc7zO6ZvTdM7pmOUAZTnJW+NXxqmd41dqJ6mLTXxrPpnV8avaIf5SvL7pndPvPpndJR9Kuu8fePvuiuhorgWjp7Mf/PRjxcFCPDkW31srioCExivv9lcwKEaHsf/7ow2Fl1T/9RkXgEhYElAoCLFtMArxwivDJJ+bR1HTKJdlEoTELCIqgEwVGSQ+hIm0NbK8WXcTEI0UPoa2NbG4y2K00JEWbZavJXkYaqo9CRHS55FcZTjKEk3NKoCYUnSQ0rWxrZbFKbKIhOKPZe1cJKzZSaQrIyULHDZmV5K4xySsDRKWOruanGtjLJXFEmwaIbDLX0hIPBUQPVFVkQkDoUNfSoDgQGKPekoxeGzA4DUvnn4bxzcZrtJyipKfPNy5w+9lnXwgqsiyHNeSVpemw4bWb9psYeq//uQZBoABQt4yMVxYAIAAAkQoAAAHvYpL5m6AAgAACXDAAAAD59jblTirQe9upFsmZbpMudy7Lz1X1DYsxOOSWpfPqNX2WqktK0DMvuGwlbNj44TleLPQ+Gsfb+GOWOKJoIrWb3cIMeeON6lz2umTqMXV8Mj30yWPpjoSa9ujK8SyeJP5y5mOW1D6hvLepeveEAEDo0mgCRClOEgANv3B9a6fikgUSu/DmAMATrGx7nng5p5iimPNZsfQLYB2sDLIkzRKZOHGAaUyDcpFBSLG9MCQALgAIgQs2YunOszLSAyQYPVC2YdGGeHD2dTdJk1pAHGAWDjnkcLKFymS3RQZTInzySoBwMG0QueC3gMsCEYxUqlrcxK6k1LQQcsmyYeQPdC2YfuGPASCBkcVMQQqpVJshui1tkXQJQV0OXGAZMXSOEEBRirXbVRQW7ugq7IM7rPWSZyDlM3IuNEkxzCOJ0ny2ThNkyRai1b6ev//3dzNGzNb//4uAvHT5sURcZCFcuKLhOFs8mLAAEAt4UWAAIABAAAAAB4qbHo0tIjVkUU//uQZAwABfSFz3ZqQAAAAAngwAAAE1HjMp2qAAAAACZDgAAAD5UkTE1UgZEUExqYynN1qZvqIOREEFmBcJQkwdxiFtw0qEOkGYfRDifBui9MQg4QAHAqWtAWHoCxu1Yf4VfWLPIM2mHDFsbQEVGwyqQoQcwnfHeIkNt9YnkiaS1oizycqJrx4KOQjahZxWbcZgztj2c49nKmkId44S71j0c8eV9yDK6uPRzx5X18eDvjvQ6yKo9ZSS6l//8elePK/Lf//IInrOF/FvDoADYAGBMGb7FtErm5MXMlmPAJQVgWta7Zx2go+8xJ0UiCb8LHHdftWyLJE0QIAIsI+UbXu67dZMjmgDGCGl1H+vpF4NSDckSIkk7Vd+sxEhBQMRU8j/12UIRhzSaUdQ+rQU5kGeFxm+hb1oh6pWWmv3uvmReDl0UnvtapVaIzo1jZbf/pD6ElLqSX+rUmOQNpJFa/r+sa4e/pBlAABoAAAAA3CUgShLdGIxsY7AUABPRrgCABdDuQ5GC7DqPQCgbbJUAoRSUj+NIEig0YfyWUho1VBBBA//uQZB4ABZx5zfMakeAAAAmwAAAAF5F3P0w9GtAAACfAAAAAwLhMDmAYWMgVEG1U0FIGCBgXBXAtfMH10000EEEEEECUBYln03TTTdNBDZopopYvrTTdNa325mImNg3TTPV9q3pmY0xoO6bv3r00y+IDGid/9aaaZTGMuj9mpu9Mpio1dXrr5HERTZSmqU36A3CumzN/9Robv/Xx4v9ijkSRSNLQhAWumap82WRSBUqXStV/YcS+XVLnSS+WLDroqArFkMEsAS+eWmrUzrO0oEmE40RlMZ5+ODIkAyKAGUwZ3mVKmcamcJnMW26MRPgUw6j+LkhyHGVGYjSUUKNpuJUQoOIAyDvEyG8S5yfK6dhZc0Tx1KI/gviKL6qvvFs1+bWtaz58uUNnryq6kt5RzOCkPWlVqVX2a/EEBUdU1KrXLf40GoiiFXK///qpoiDXrOgqDR38JB0bw7SoL+ZB9o1RCkQjQ2CBYZKd/+VJxZRRZlqSkKiws0WFxUyCwsKiMy7hUVFhIaCrNQsKkTIsLivwKKigsj8XYlwt/WKi2N4d//uQRCSAAjURNIHpMZBGYiaQPSYyAAABLAAAAAAAACWAAAAApUF/Mg+0aohSIRobBAsMlO//Kk4soosy1JSFRYWaLC4qZBYWFRGZdwqKiwkNBVmoWFSJkWFxX4FFRQWR+LsS4W/rFRb/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////VEFHAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAU291bmRib3kuZGUAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAMjAwNGh0dHA6Ly93d3cuc291bmRib3kuZGUAAAAAAAAAACU=');"
                +"sound.play(); }";
        fv.setCustomScript(script);
        fv.setTimeForRendering(500);
        fv.setNavigationAreaWidthRatio(0.0f);

        fv.setCurlQuality(0.6f);
        fv.setTimeForRendering(500);
        //fv.setNavigationAreaWidthRatio(0.0f);

        fv.setImmersiveMode(true);

        fv.setLicenseKey("ae98-8a9a-fee4-4de4");
        fv.setRotationLocked(true);
    }

    private class KeyDelegate implements KeyListener {
        @Override
        public String getKeyForEncryptedData(String uuidForContent, String contentName, String uuidForEpub) {
            // TODO Auto-generated method stub
            return "test";
        }

        @Override
        public Book getBook() {
            // TODO Auto-generated method stub
            return fv.getBook();
        }
    }


    private void getBookInformation () {
        SkyProvider skyProvider = new SkyProvider();
        bi = new BookInformation();
        bi.setFileName(fileManager.bookFileName(BookInfo.getContentsId()));
        bi.setBaseDirectory(fileManager.bookDirectory(BookInfo.getContentsId()));
        bi.setCoverPath("");
        bi.setContentProvider(skyProvider);
        skyProvider.setBook(bi.getBook());

        bi.makeInformation();
    }*/

}
