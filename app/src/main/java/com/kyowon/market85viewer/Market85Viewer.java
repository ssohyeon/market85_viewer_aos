package com.kyowon.market85viewer;

import com.skytree.epub.Book;

public class Market85Viewer {
    private static BookInfo bookInfo;

    public Market85Viewer (BookInfo bookInfo) {
        this.bookInfo = bookInfo;
    }

    public static BookInfo getBookInfo() {
        if(bookInfo == null) {
            bookInfo = new BookInfo();
        }
        return bookInfo;
    }


    public class BookmarkHelper {

        public void Bookmark(int page) {

        }

        public void BookmarkDelete(BookmarkInfo bookmarkInfo) {

        }

        public void BookmarkList() {

        }


    }
}
