package com.kyowon.market85viewer;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.util.Pair;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.kyowon.market85viewer.Magazine.MagazineActivity;
import com.kyowon.market85viewer.RestFul.API.Download;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class TestLoadingActivity extends Activity {
    TextView txtProgressPercent;
    ProgressBar progressBar;
    Button btnDownloadFile, btnDeleteFile;

    DownloadZipFileTask downloadZipFileTask;
    private static final String TAG = "TestLoadingActivity";

    private String contentsId;
    private int page;

    private FileManager fileManager;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_test_loading);

        fileManager = new FileManager(this);

        if(BookInfo.getContentsId()==null)
            BookInfo.setContentsId(getIntent().getStringExtra("contentsId"));

        page = getIntent().getIntExtra("page", 0);
        BookInfo.setCookie("kwopSvcJWT=SDP+eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJzdmMiLCJqdGkiOiJcblx1MDAxRiRcdTAwMDNcdTAwMThcdTAwMTlcdTAwMUEsIiwiYXVkIjoiMTcyLjE2LjMxLjEzMSIsImlzcyI6IkktT04iLCJpYXQiOjE1ODk2OTIyNjEsImV4cCI6MzI0NzIxMTE2MDB9.H7_lt8MVche2YVdkkoamG8YOEPAjohYXH--EvRYhYW8b09Z2n08PSIgcuNssrLNgrt1Znb2x5Ud9GT2p6iLbOA");


        //askForPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE, 101);

        txtProgressPercent = findViewById(R.id.txtProgressPercent);
        progressBar = findViewById(R.id.progressBar);
        btnDownloadFile = findViewById(R.id.btnStart);
        btnDeleteFile = findViewById(R.id.btnDelete);


        String viewerDirectory = fileManager.viewerDirectory(contentsId);
        fileManager.makeFileDirectory(viewerDirectory);

        String bookDirectory = fileManager.bookDirectory(contentsId);
        fileManager.makeFileDirectory(bookDirectory);

        if(fileManager.isBookSaved(contentsId)){

        }

        /*if(fileManager.isBookSaved(contentsId)) {

            Intent intent = new Intent(TestLoadingActivity.this, MagazineActivity.class);
            intent.putExtra("contentsId", contentsId);
            intent.putExtra("page", page);
            startActivity(intent);
        } else {
            downloadFile();
        }*/

        btnDownloadFile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //downloadFile();
                Intent tutorialIntent = new Intent(TestLoadingActivity.this, LoadingActivity.class);
                startActivity(tutorialIntent);
            }
        });


        btnDeleteFile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //getFile();
                Intent intent = new Intent(TestLoadingActivity.this, MagazineActivity.class);
                intent.putExtra("contentsId", contentsId);
                intent.putExtra("page", page);
                startActivity(intent);
            }
        });


    }

    private void downloadFile() {

        Download downloadService = createService(Download.class, "http://scs.skyepub.net/");

        Call<ResponseBody> call = downloadService.downloadFileByUrl("samples/Doctor.epub");

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, final Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    Log.d(TAG, "Got the body for the file");
                    Toast.makeText(getApplicationContext(), "Downloading...", Toast.LENGTH_SHORT).show();
                    downloadZipFileTask = new DownloadZipFileTask();
                    downloadZipFileTask.execute(response.body());
                } else {
                    Log.d(TAG, "Connection failed " + response.errorBody());
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                t.printStackTrace();
                Log.e(TAG, t.getMessage());
            }
        });
    }

    public <T> T createService(Class<T> serviceClass, String baseUrl) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(baseUrl)
                .client(new OkHttpClient.Builder().build())
                .build();
        return retrofit.create(serviceClass);
    }

    private class DownloadZipFileTask extends AsyncTask<ResponseBody, Pair<Integer, Long>, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected String doInBackground(ResponseBody... urls) {
            //Copy you logic to calculate progress and call
            {
                String filePath = fileManager.bookFilePath(contentsId);

                Log.d(TAG, "SH Test - filePath : " + filePath);
                saveToDisk(urls[0], filePath);
            }
            return null;
        }

        protected void onProgressUpdate(Pair<Integer, Long>... progress) {

            Log.d("API123", progress[0].second + " ");

            if (progress[0].first == 100)
                Toast.makeText(getApplicationContext(), "File downloaded successfully", Toast.LENGTH_SHORT).show();


            if (progress[0].second > 0) {
                int currentProgress = (int) ((double) progress[0].first / (double) progress[0].second * 100);
                progressBar.setProgress(currentProgress);

                txtProgressPercent.setText("Progress " + currentProgress + "%");

            }

            if (progress[0].first == -1) {
                Toast.makeText(getApplicationContext(), "Download failed", Toast.LENGTH_SHORT).show();
            }

        }

        public void doProgress(Pair<Integer, Long> progressDetails) {
            publishProgress(progressDetails);
        }

        @Override
        protected void onPostExecute(String result) {

        }
    }

    private void saveToDisk(ResponseBody body, String filePath) {

        try {
            InputStream inputStream = null;

            FileOutputStream localFileOutputStream =null;

            try {
                inputStream = body.byteStream();
                localFileOutputStream = new FileOutputStream(filePath);

                byte data[] = new byte[4096];
                int count;
                int progress = 0;
                long fileSize = body.contentLength();
                Log.d(TAG, "File Size=" + fileSize);
                while ((count = inputStream.read(data)) != -1) {
                    localFileOutputStream.write(data, 0, count);
                    //outputStream.write(data, 0, count);
                    progress += count;
                    Pair<Integer, Long> pairs = new Pair<>(progress, fileSize);
                    downloadZipFileTask.doProgress(pairs);
                    Log.d(TAG, "Progress: " + progress + "/" + fileSize + " >>>> " + (float) progress / fileSize);
                }

                localFileOutputStream.flush();
                //outputStream.flush();

                //Log.d(TAG, destinationFile.getParent());
                Pair<Integer, Long> pairs = new Pair<>(100, 100L);
                downloadZipFileTask.doProgress(pairs);
                return;
            } catch (IOException e) {
                e.printStackTrace();
                Pair<Integer, Long> pairs = new Pair<>(-1, Long.valueOf(-1));
                downloadZipFileTask.doProgress(pairs);
                Log.d(TAG, "Failed to save the file!");
                return;
            } finally {
                if (inputStream != null) inputStream.close();
                if (localFileOutputStream != null) localFileOutputStream.close();
                //if (outputStream != null) outputStream.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
            Log.d(TAG, "Failed to save the file!");
            return;
        }
    }

    //SH Test
    public void getFile() {

        String directory =  getFilesDir().getAbsolutePath() + "/11111111/";
        //String directory = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).toString();

        File file = new File(directory);
        File list[] = file.listFiles();

        List fileList = new ArrayList();

        for(int i=0; i<list.length; i++) {
            Log.d(TAG, "SH Test - getPath : " + list[i].getPath().toString());
            Log.d(TAG, "SH Test - getName : " + list[i].getName().toString());

            if((list[i].getName()).contains(".epub")) {
                fileList.add(list[i].getPath());
            }
        }

        Log.d(TAG, "SH Test --------------------");
        for(int i=0; i<fileList.size(); i++) {
            Log.d(TAG, "SH Test - file Path : " + fileList.get(i).toString().toString());
            //ls.installBook(fileList.get(i).toString());

            // install Book

        }

    }

/*    private void askForPermission(String permission, Integer requestCode) {
        if (ContextCompat.checkSelfPermission(MainActivity.this, permission) != PackageManager.PERMISSION_GRANTED) {


            if (ActivityCompat.shouldShowRequestPermissionRationale(MainActivity.this, permission)) {
                ActivityCompat.requestPermissions(MainActivity.this, new String[]{permission}, requestCode);

            } else {
                ActivityCompat.requestPermissions(MainActivity.this, new String[]{permission}, requestCode);
            }
        } else if (ContextCompat.checkSelfPermission(MainActivity.this, permission) == PackageManager.PERMISSION_DENIED) {
            Toast.makeText(getApplicationContext(), "Permission was denied", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (ActivityCompat.checkSelfPermission(this, permissions[0]) == PackageManager.PERMISSION_GRANTED) {

            if (requestCode == 101)
                Toast.makeText(this, "Permission granted", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, "Permission denied", Toast.LENGTH_SHORT).show();
        }
    }*/
}


