package com.kyowon.market85viewer;

public class BookPageInfo {
    private int page;
    private boolean bookmark;
    private BookmarkInfo bookmarkInfo;
    private String chapterTitle;

    public int getPage() {
        return page;
    }

    public boolean isBookmark() {
        return bookmark;
    }

    public BookmarkInfo getBookmarkInfo() {
        return bookmarkInfo;
    }

    public String getChapterTitle() {
        return chapterTitle;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public void setBookmark(boolean bookmark) {
        this.bookmark = bookmark;
    }

    public void setBookmarkInfo(BookmarkInfo bookmarkInfo) {
        this.bookmarkInfo = bookmarkInfo;
    }

    public void setChapterTitle(String chapterTitle) {
        this.chapterTitle = chapterTitle;
    }
}
