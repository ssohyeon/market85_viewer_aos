package com.kyowon.market85viewer;

public class BookInfo {
    public static String contentsId = null;
    public static String cookie = null;


    public static String getCookie() {
        return cookie;
    }

    public static String getContentsId() {
        return contentsId;
    }

    public static void setCookie(String cookie) {
        BookInfo.cookie = cookie;
    }

    public static void setContentsId(String contentsId) {
        BookInfo.contentsId = contentsId;
    }


}

